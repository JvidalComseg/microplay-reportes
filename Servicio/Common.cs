﻿using Infraestructura.Procedimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Servicio
{
    public class Common
    {
        private RSPSample repositorio;

        public Common()
        {
            repositorio = new RSPSample();
        }



        public Dictionary<string, string> Info()
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            string sp_name = "SP_INFO";


            var rows =  repositorio.Obtener(sp_name);
            foreach (var item in rows)
            {
                ret[item["id"]] = item["valor"];
            }

            return ret;

        }

        public Dictionary<string, string> ECommerce(  string UserName )
        {



            string sp_name = "ECCOMERCE_LIST_X_USER";
            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("usuario", UserName);

            var rows = repositorio.Obtener(sp_name, Parameters);

            Dictionary<string, string> ret = new Dictionary<string, string>();

            foreach (var item in rows)
            {
                ret[item["NOMBRE"]] = item["DETALLE"];
            }

            return ret;

        }

        public string Roles_Pagina(string pagina)
        {
            string sp_name = "SP_ROLES_BY_PAGE";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("page", pagina);

            Dictionary<string, string> ret = new Dictionary<string, string>();


            var items = repositorio.Obtener(sp_name, Parameters);

            List<string> aRet = new List<string>();
            foreach (var item in items)
            {
                aRet.Add( item["ROL_NAME"] );
            }


            return string.Join(",", aRet);
        }



        public Dictionary<string, string> SucursalesXEmpresa(string empresa)
        {
            string sp_name = "SP_LOCALES_EMPRESA";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("empresa", empresa);

            Dictionary<string, string> ret = new Dictionary<string, string>();


            var items = repositorio.Obtener(sp_name, Parameters);

            foreach (var item in items)
            {
                ret[item["CODIGO"]] = item["DESCRIPCION"]; 
            }

            return ret;

        }

        public Dictionary<string, string> SucursalesXUsuario(string usuario)
        {
            string sp_name = "SP_LOCALES_USUARIO";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("Vendedor", usuario);

            Dictionary<string, string> ret = new Dictionary<string, string>();


            var items = repositorio.Obtener(sp_name, Parameters);

            foreach (var item in items)
            {
                ret[item["local"]] = item["local"];
            }

            return ret;
        }


        public List<SelectListItem> ListaAnios()
        {

            int anio = DateTime.Now.Year;

            List<SelectListItem> ret = new List<SelectListItem>();

            ret.Add( new SelectListItem { Text = string.Empty, Value = "00"} ) ;
            for ( int i = anio; i > 2014; i-- )
            {
                string s = i.ToString();
                ret.Add(new SelectListItem { Text = s, Value = s });
            }


            return ret ;
        }

        public List<SelectListItem> ListaMeses()
        {
            return new List<SelectListItem>
                {
                    new SelectListItem { Text = string.Empty, Value = "00"},
                    new SelectListItem { Text = "Enero", Value = "01"},
                    new SelectListItem { Text = "Febrero", Value = "02"},
                    new SelectListItem { Text = "Marzo", Value = "03"},
                    new SelectListItem { Text = "Abril", Value = "04"},
                    new SelectListItem { Text = "Mayo", Value = "05"},
                    new SelectListItem { Text = "Junio", Value = "06"},
                    new SelectListItem { Text = "Julio", Value = "07"},
                    new SelectListItem { Text = "Agosto", Value = "08"},
                    new SelectListItem { Text = "Septiembre", Value = "09"},
                    new SelectListItem { Text = "Octubre", Value = "10"},
                    new SelectListItem { Text = "Noviembre", Value = "11"},
                    new SelectListItem { Text = "Diciembre", Value = "12"}
                };
        }


        public List<SelectListItem> ListaDias()
        {
            return new List<SelectListItem>
                {
                    new SelectListItem { Text = string.Empty, Value = "00"},
                    new SelectListItem { Text = "01", Value = "01"},
                    new SelectListItem { Text = "02", Value = "02"},
                    new SelectListItem { Text = "03", Value = "03"},
                    new SelectListItem { Text = "04", Value = "04"},
                    new SelectListItem { Text = "05", Value = "05"},
                    new SelectListItem { Text = "06", Value = "06"},
                    new SelectListItem { Text = "07", Value = "07"},
                    new SelectListItem { Text = "08", Value = "08"},
                    new SelectListItem { Text = "09", Value = "09"},
                    new SelectListItem { Text = "10", Value = "10"},

                    new SelectListItem { Text = "11", Value = "11"},
                    new SelectListItem { Text = "12", Value = "12"},
                    new SelectListItem { Text = "13", Value = "13"},
                    new SelectListItem { Text = "14", Value = "14"},
                    new SelectListItem { Text = "15", Value = "15"},
                    new SelectListItem { Text = "16", Value = "16"},
                    new SelectListItem { Text = "17", Value = "17"},
                    new SelectListItem { Text = "18", Value = "18"},
                    new SelectListItem { Text = "19", Value = "19"},
                    new SelectListItem { Text = "20", Value = "20"},

                    new SelectListItem { Text = "21", Value = "21"},
                    new SelectListItem { Text = "22", Value = "22"},
                    new SelectListItem { Text = "23", Value = "23"},
                    new SelectListItem { Text = "24", Value = "24"},
                    new SelectListItem { Text = "25", Value = "25"},
                    new SelectListItem { Text = "26", Value = "26"},
                    new SelectListItem { Text = "27", Value = "27"},
                    new SelectListItem { Text = "28", Value = "28"},
                    new SelectListItem { Text = "29", Value = "29"},
                    new SelectListItem { Text = "30", Value = "30"},

                    new SelectListItem { Text = "31", Value = "31"}

            };
        }


    }
}
