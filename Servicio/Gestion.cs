﻿using Infraestructura.Procedimientos;
using Servicio.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicio
{
    public class Gestion : IDisposable
    {
        private RSPSample repositorio;

        public Gestion()
        {
            repositorio = new RSPSample();
        }


        #region Margen Por Documento

        public IEnumerable<Dictionary<string, string>> MargenPorDocumento(string empresa, int ano, string mes, string sucursal)
        {
            string sp_name = "DETALLE_MARGEN_PORDOCUMENTO";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();
            
            Parameters.Add("empresa", empresa);
            Parameters.Add("ano", ano);
            Parameters.Add("mes", mes);
            Parameters.Add("sucursal", sucursal);

            var DataIn = repositorio.Obtener(sp_name, Parameters);
            var ret = Tools.ClearResults( "WH_",   MargenPorDocumento_FitData( DataIn ));

            return Totalizar(ret);
        }


        private IEnumerable<Dictionary<string, string>> MargenPorDocumento_FitData(IEnumerable<Dictionary<string, string>> DataIn)
        {
            List<Dictionary<string, string>> DataOut = new List<Dictionary<string, string>>();
            string old_local = "";
            string old_periodo = "";
            double precio_sum = 0;
            double costo_sum = 0;
            double margen_sum = 0;
            double nroDocumentos_sum = 0;
            double Unidades_sum = 0;

            foreach ( var row in DataIn )
            {

                if(  ! row["Local"].Equals(old_local) )
                {
                    if(precio_sum > 0 )
                    {
                        Dictionary<string, string> dout2 = new Dictionary<string, string>();
                        dout2["Local"] = old_local;
                        dout2["PeriodoLibro"] = old_periodo;
                        dout2["Clase"] = "TOTAL";


                        dout2["Precio"] = precio_sum.ToString();
                        dout2["Costo"] = ((long)costo_sum).ToString();
                        dout2["Margen"] = ((long)margen_sum).ToString();
                        dout2["NroDocumentos"] = ((long)nroDocumentos_sum).ToString(); ;
                        dout2["MediaDtos"] = ((long)(precio_sum / nroDocumentos_sum)).ToString();
                        dout2["Unidades"] = ((long)Unidades_sum).ToString();
                        dout2["UnidadesDctos"] = string.Format("{0:0.00}", Unidades_sum / nroDocumentos_sum);

                        double margen_porcentual = margen_sum / precio_sum * 100;
                        dout2["MargenPorcentual"] = string.Format("{0:0.00}", margen_porcentual) ;

                        DataOut.Add(dout2);
                    }

                    precio_sum = 0;
                    costo_sum = 0;
                    margen_sum = 0;
                    nroDocumentos_sum = 0;
                    Unidades_sum = 0;
                    old_local = row["Local"];
                    old_periodo = row["PeriodoLibro"];
                }

                double.TryParse(row["Precio"], out double precio);
                double.TryParse(row["Costo"], out double costo);
                double.TryParse(row["Margen"], out double margen);
                double.TryParse(row["MargenPorcentual"], out double margenporcentual);
                double.TryParse(row["NroDocumentos"], out double nroDocumentos);
                double.TryParse(row["MediaDtos"], out double MediaDtos);
                double.TryParse(row["unidades"], out double Unidades);
                

                precio_sum += precio;
                costo_sum += costo;
                margen_sum += margen;
                nroDocumentos_sum += nroDocumentos;
                Unidades_sum += Unidades;

                Dictionary<string, string> dout = new Dictionary<string, string>();
                dout["Local"] = row["Local"];
                dout["PeriodoLibro"] = row["PeriodoLibro"];
                dout["Clase"] = row["Clase"];


                dout["Precio"] =precio.ToString();
                dout["Costo"] = ((long)costo).ToString();
                dout["Margen"] = ((long)margen).ToString();
                dout["MargenPorcentual"] = string.Format("{0:0.00}", margenporcentual) ;
                dout["NroDocumentos"] = ((long)nroDocumentos).ToString(); 
                dout["MediaDtos"] = ((long)MediaDtos).ToString();
                dout["Unidades"] = ((long)Unidades).ToString();
                dout["UnidadesDctos"] = string.Format("{0:0.00}", Unidades / nroDocumentos);


                DataOut.Add(dout);
            }


            if (precio_sum > 0)
            {
                Dictionary<string, string> dout2 = new Dictionary<string, string>();
                dout2["Local"] = old_local;
                dout2["PeriodoLibro"] = old_periodo;
                dout2["Clase"] = "TOTAL";


                dout2["Precio"] = precio_sum.ToString();
                dout2["Costo"] = ((long)costo_sum).ToString();
                dout2["Margen"] = ((long)margen_sum).ToString();
                dout2["NroDocumentos"] = ((long)nroDocumentos_sum).ToString(); 
                dout2["MediaDtos"] = ((long) (precio_sum/ nroDocumentos_sum) ).ToString();
                dout2["Unidades"] = ((long)Unidades_sum).ToString();

                double margen_porcentual = margen_sum / precio_sum * 100;
                dout2["MargenPorcentual"] = string.Format("{0:0.00}", margen_porcentual) ;
                dout2["UnidadesDctos"] = string.Format("{0:0.00}", Unidades_sum / nroDocumentos_sum);

                DataOut.Add(dout2);
            }

            return DataOut; 
        }


        private string Acumular(string s, string key, string valor)
        {
            if( !key.Contains("ACUMULADO_"))
            {
                long totalvalue = 0; long value = 0;

                try { long.TryParse(s, out totalvalue); } catch (Exception e) { }
                try { long.TryParse(valor, out value); } catch (Exception e) { }



                return (totalvalue + value).ToString();

            }
            return "";
        }



        private List<Dictionary<string, string>> Totalizar(IEnumerable<Dictionary<string, string>> Data_In)
        {
            List<Dictionary<string, string>> ret = new List<Dictionary<string, string>>();

            var grupos = Data_In
                .Where(w => !w["Clase"].Equals("TOTAL"))
                .GroupBy(g => g["Clase"])
                .ToList() ;


            string periodo = "";
            long __precio = 0, __costo = 0, __margen = 0, __NroDocumentos = 0, __Unidades = 0;
            foreach (var grp in grupos)
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();

                long _precio=0, _costo=0, _margen=0, _NroDocumentos=0, _Unidades=0;

                foreach (var e in grp.ToList())
                {
                    periodo = e["PeriodoLibro"];

                    long.TryParse(e["Precio"], out long precio);
                    long.TryParse(e["Costo"], out long costo);
                    long.TryParse(e["Margen"], out long margen);
                    long.TryParse(e["NroDocumentos"], out long NroDocumentos);
                    long.TryParse(e["Unidades"], out long Unidades);

                    _precio += precio;
                    _costo += costo;
                    _margen += margen;
                    _NroDocumentos += NroDocumentos;
                    _Unidades += Unidades;

                    __precio += precio;
                    __costo += costo;
                    __margen += margen;
                    __NroDocumentos += NroDocumentos;
                    __Unidades += Unidades;

                }

                dict["Local"] = "RESUMEN";
                dict["Clase"] = grp.Key;
                dict["PeriodoLibro"] = periodo;
                dict["Precio"] = _precio.ToString();
                dict["Costo"] = _costo.ToString();
                dict["Margen"] = _margen.ToString();

                dict["NroDocumentos"] = _NroDocumentos.ToString();
                dict["MediaDtos"] = ((long)(_precio / _NroDocumentos)).ToString();
                dict["Unidades"] = _Unidades.ToString();

                double margen_porcentual = (double)_margen / (double)_precio * 100;
                dict["MargenPorcentual"] = string.Format("{0:0.00}", margen_porcentual);
                dict["UnidadesDctos"] = string.Format("{0:0.00}", ((double)_Unidades / (double)_NroDocumentos) );

                ret.Add(dict);

            }

            Dictionary<string, string> dict0 = new Dictionary<string, string>();
            dict0["Local"] = "RESUMEN";
            dict0["Clase"] = "TOTAL";
            dict0["PeriodoLibro"] = periodo;
            dict0["Precio"] = __precio.ToString();
            dict0["Costo"] = __costo.ToString();
            dict0["Margen"] = __margen.ToString();

            dict0["NroDocumentos"] = __NroDocumentos.ToString();
            dict0["MediaDtos"] = "0";
            try { dict0["MediaDtos"] = ((long)(__precio / __NroDocumentos)).ToString(); } catch (Exception e) { }
            
            dict0["Unidades"] = __Unidades.ToString();

            double __margen_porcentual = 0;
            try { __margen_porcentual = (double)__margen / (double)__precio * 100; } catch (Exception e) { }
            dict0["MargenPorcentual"] = string.Format("{0:0.00}", __margen_porcentual);

            dict0["UnidadesDctos"] = string.Format("{0:0.00}",  0.0 );
            try { dict0["UnidadesDctos"] = string.Format("{0:0.00}", ((double)__Unidades / (double)__NroDocumentos));  } catch (Exception e) { }
            

            ret.Add(dict0);


            foreach( var row in Data_In )
            {
                ret.Add(row);
            }

            return ret;
        }

        #endregion

        public void Dispose()
        {
        }
    }
}
