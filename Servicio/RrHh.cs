﻿using Infraestructura.Procedimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicio
{
    public class RrHh : IDisposable
    {

        private RSPSample repositorio;

        public RrHh()
        {
            repositorio = new RSPSample();
        }


        #region Ventas Locales
        public IEnumerable<Dictionary<string, string>> VentasLocales( string empresa, string sucursales, int anio, string mes)
        {
            string sp_name = "SP_VENTAS_LOCALES_X_PERIODO";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("empresa", empresa);
            Parameters.Add("sucursal", sucursales);
            Parameters.Add("ano", anio);
            Parameters.Add("mes", mes);

            return repositorio.Obtener(sp_name, Parameters);
        }
        #endregion 


        #region Vendedores
        public IEnumerable<Dictionary<string, string>> Vendedores(string empresa, string vendedor)
        {
            string sp_name = "SP_VENDEDORES_BUSCAR";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("empresa", empresa);
            Parameters.Add("vendedor", vendedor);

            return repositorio.Obtener(sp_name, Parameters);
        }
        #endregion

        #region Ventas Vendedor

        public IEnumerable<Dictionary<string, string>> VentaVendedor(string vendedor, string fechaInicio, string fechaFinal)
        {
            string sp_name = "SP_VENTAS_VENDEDOR";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("vendedor", vendedor);
            Parameters.Add("fecha_inicio", fechaInicio);
            Parameters.Add("fecha_final", fechaFinal);

            return repositorio.Obtener(sp_name, Parameters);
            // return FitDataVentas(repositorio.Obtener(sp_name, Parameters));
        }

        public IEnumerable<Dictionary<string, string>> VentaVendedorGE(string vendedor, string fechaInicio, string fechaFinal)
        {
            string sp_name = "SP_VENTAS_VENDEDOR_GE";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("vendedor", vendedor);
            Parameters.Add("fecha_inicio", fechaInicio);
            Parameters.Add("fecha_final", fechaFinal);

            return repositorio.Obtener(sp_name, Parameters);
            // return FitDataVentas(repositorio.Obtener(sp_name, Parameters));
        }




        private IEnumerable<Dictionary<string, string>> FitDataVentas(IEnumerable<Dictionary<string, string>> DataIn)
        {
            List<Dictionary<string, string>> DataOut = new List<Dictionary<string, string>>();

            return DataOut;
        }



        public IEnumerable<Dictionary<string, string>> VentaVendedorResumen(string vendedor, string fechaInicio, string fechaFinal)
        {
            string sp_name = "SP_VENTAS_VENDEDOR_TOTALES";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("vendedor", vendedor);
            Parameters.Add("fecha_inicio", fechaInicio);
            Parameters.Add("fecha_final", fechaFinal);

            return repositorio.Obtener(sp_name, Parameters);
            // return FitDataVentasResumen(repositorio.Obtener(sp_name, Parameters));
        }

        public IEnumerable<Dictionary<string, string>> VentaVendedorGEResumen(string vendedor, string fechaInicio, string fechaFinal)
        {
            string sp_name = "SP_VENTAS_VENDEDOR_GE_TOTALES";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("vendedor", vendedor);
            Parameters.Add("fecha_inicio", fechaInicio);
            Parameters.Add("fecha_final", fechaFinal);

            return repositorio.Obtener(sp_name, Parameters);
            // return FitDataVentasResumen(repositorio.Obtener(sp_name, Parameters));
        }



        private IEnumerable<Dictionary<string, string>> FitDataVentasResumen(IEnumerable<Dictionary<string, string>> DataIn)
        {
            List<Dictionary<string, string>> DataOut = new List<Dictionary<string, string>>();

            return DataOut;
        }



        #endregion

        #region Comisiones

        public IEnumerable<Dictionary<string, string>> ComisionesLocal(string empresa, int ano, string mes, string sucursal)
        {
            string sp_name = "SP_COMISIONES_LOCAL";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("empresa", empresa);
            Parameters.Add("ano", ano);
            Parameters.Add("mes", mes);
            Parameters.Add("sucursal", sucursal);

            return FitDataComision(ano, repositorio.Obtener(sp_name, Parameters));
        }


        private IEnumerable<Dictionary<string, string>> FitDataComision(int anio, IEnumerable<Dictionary<string, string>> DataIn)
        {
            List<Dictionary<string, string>> DataOut = new List<Dictionary<string, string>>();
            foreach( var row in DataIn)
            {
                Dictionary<string, string> fRow = new Dictionary<string, string>();
                int precio=0, costo=0, margen = 0, doctos = 0 ;
                double margenPorcentual = 0;
                int.TryParse(row["precio"], out precio);
                int.TryParse(row["costo"], out costo);
                int.TryParse(row["docs"], out doctos);
                margen = precio - costo;
                if(precio != 0 )
                    margenPorcentual = (double) margen / (double) precio * 100.0 ;

                fRow["Clase"] = row["Clase"];
                fRow["Local"] = row["local"];
                fRow["Precio"] = row["precio"];
                fRow["Costo"] = row["costo"];
                fRow["Margen"] =  margen.ToString();
                fRow["MargenPorcentual"] = string.Format("{0:0.00}", margenPorcentual);
                fRow["NroDocumentos"] = doctos.ToString();

                fRow["MediaDtos"] = "" ;
                fRow["Unidades"] = "" ;
                fRow["UnidadesDctos"] = "";


                /*
                        <td align="right" @Html.Raw("class='Number " + useclass + "'")>  @Html.Raw(row["MediaDtos"]) </td>
                        <td align="right" @Html.Raw("class='Number " + useclass + "'")>  @Html.Raw(row["Unidades"]) </td>
                        <td align="right" @Html.Raw("class='Media " + useclass + "'")>  @Html.Raw(row["UnidadesDctos"]) </td>
                 * */

                DataOut.Add(fRow);
            }
                

            return DataOut;
        }

        #endregion

        public void Dispose()
        {
           
        }
    }
}
