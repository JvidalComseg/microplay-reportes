﻿using Infraestructura.Procedimientos;
using Servicio.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicio
{
    public class Admin : IDisposable
    {
        private RSPSample repositorio;

        public Admin()
        {
            repositorio = new RSPSample();
        }

        #region USUARIOS

            public IEnumerable<Dictionary<string, string>> Usuarios_Listado()
            {

                string sp_name = "SP_ADM_USUARIOS_LIST";

                return repositorio.Obtener(sp_name);
            }


        #endregion

        #region REPORTES
        public IEnumerable<Dictionary<string, string>> Reportes_Listado()
        {

            string sp_name = "SP_ADM_REPORTES_LIST";

            return repositorio.Obtener(sp_name);
        }
        #endregion

        #region ROLES
        public IEnumerable<Dictionary<string, string>> Roles_Listado()
        {

            string sp_name = "SP_ADM_ROLES_LIST";

            return repositorio.Obtener(sp_name);
        }

        #endregion

        public void Dispose()
        {
        }
    }
}
