﻿using Infraestructura.Procedimientos;
using Servicio.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicio
{
    public class Ventas : IDisposable
    {
        private RSPSample repositorio;

        public Ventas()
        {
            repositorio = new RSPSample();
        }


        public IEnumerable<Dictionary<string, string>> VentasLocalPorVendedor(string empresa,int ano, string mes, string sucursal)
        {
            int iMes = 0;
            int.TryParse(mes, out iMes);
            string sp_name = "SP_VENTAS_LOCAL_X_VENDEDOR";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("empresa", empresa);
            Parameters.Add("anio", ano);
            Parameters.Add("mes", iMes);
            Parameters.Add("sucursal", sucursal);

            var ret = repositorio.Obtener(sp_name, Parameters);

            return ret;
        }


        
        public Dictionary<string, string> Vendedor( string vendedor, int ano, string mes)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

              int iMes = 0;
            int.TryParse(mes, out iMes);
            string sp_name = "SP_VENTAS_X_VENDEDOR";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("anio", ano);
            Parameters.Add("mes", iMes);
            Parameters.Add("vendedor", vendedor);

            var t = repositorio.Obtener(sp_name, Parameters);
            foreach( var t0 in t )
            {
                ret[t0["rem"]] = t0["VentaTotalDia"];
            }


            return ret;
        }



        public IEnumerable<Dictionary<string, string>> Mes_Web( string empresa, int ano, string mes, string sucursal)
        {
            string sp_name = "SP_RPT_VENTAS_WEB";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("empresa", empresa);
            Parameters.Add("ano", ano);
            Parameters.Add("mes", mes);
            Parameters.Add("sucursal", sucursal);

            var ret = FitData(ano, mes,  repositorio.Obtener(sp_name, Parameters));

            return Totalizar(ano, ret);
        }



       public IEnumerable<Dictionary<string, string>> Mes_Local( string empresa, int ano, string mes, string sucursal)
        {
            string sp_name = "SP_RPT_VENTAS_LOCAL_X_DIA";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("empresa", empresa);
            Parameters.Add("ano", ano);
            Parameters.Add("mes", mes);
            Parameters.Add("sucursal", sucursal);

            var data = repositorio.Obtener(sp_name, Parameters);

            var ret = FitData(ano, mes, data);

            return Totalizar(ano, ret);
        }




        private IEnumerable<Dictionary<string, string>> FitData(int anio, string mes,  IEnumerable<Dictionary<string, string>> DataIn)
        {
            int.TryParse(mes, out int _mes);

            List<Dictionary<string, string>> DataOut = new List<Dictionary<string, string>>();

            if( DataIn.Count() <= 0  )
            {
                return DataOut;
            }

            List<long> Totales_year = new List<long>();
            List<long> Totales_oldyear = new List<long>();
            long acumulado = 0;
            long acumulado_oldyear = 0;
            for (int i = 0; i <= 31; i++)
            {
                DataOut.Add(new Dictionary<string, string>());
                Totales_year.Add(0L);
                Totales_oldyear.Add(0L);
            }

            var Locales = DataIn.GroupBy(n =>  n["Local"].ToUpper() ).ToList();

            //  DataOut[0]["mes"] = "Mes";
            DataOut[0]["dia"] = "Dia";
            foreach (var t in Locales)
            {
                DataOut[0][t.Key.ToUpper() + "_" + (anio - 1).ToString()] = t.Key + "_" + (anio - 1).ToString();
                DataOut[0][t.Key.ToUpper() + "_" + anio.ToString()] = t.Key + "_" + anio.ToString();

                DataOut[0][t.Key.ToUpper() + "_ACUM_" + (anio - 1).ToString()] = t.Key + "_ACUM_" + (anio - 1).ToString();
                DataOut[0][t.Key.ToUpper() + "_ACUM_" + anio.ToString()] = t.Key + "_ACUM_" + anio.ToString();

                DataOut[0][t.Key.ToUpper() + "_VAR"] = t.Key + "_VAR";
                DataOut[0][t.Key.ToUpper() + "_VARP"] = t.Key + "_VAR%";
            }
            DataOut[0]["TOTALES_" + (anio - 1).ToString()] = "TOTALES_" + (anio - 1).ToString();
            DataOut[0]["TOTALES_" + anio.ToString()] = "TOTALES_" + anio.ToString();
            DataOut[0]["TOTALES_VAR"] = "TOTALES_VAR";
            DataOut[0]["TOTALES_VARP"] = "TOTALES_VAR%";

            DataOut[0]["ACUMULADO_" + (anio - 1).ToString()] = "ACUMULADO_" + (anio - 1).ToString();
            DataOut[0]["ACUMULADO_" + anio.ToString()] = "ACUMULADO_" + anio.ToString();
            DataOut[0]["ACUMULADO_VAR"] = "ACUMULADO_VAR";
            DataOut[0]["ACUMULADO_VARP"] = "ACUMULADO_VAR %";



            foreach (var tLocal in Locales)
            {
                long acum_local = 0;
                long acumAnterior_local = 0;
                var local = tLocal.OrderBy(o => int.Parse(o["dia"])).ToList();
                foreach (var el in local)
                {
                    int ix = int.Parse(el["dia"]);

                    // DataOut[ix]["mes"] = el["mes"];
                    DataOut[ix]["dia"] = el["dia"];

                    int.TryParse(el["dia"], out int _dia);

                    long.TryParse(el["TotalActual"], out long monto_actual);
                    long.TryParse(el["TotalAnterior"], out long monto_anterior);
                    double.TryParse(el["Variacion"], out double variacion );

                    acum_local += monto_actual;
                    acumAnterior_local += monto_anterior;


                    Totales_year[ix] += monto_actual;
                    Totales_oldyear[ix] += monto_anterior;

                    DataOut[ix][el["Local"].ToUpper() + "_" + (anio - 1).ToString()] = monto_anterior.ToString();
                    DataOut[ix][el["Local"].ToUpper() + "_ACUM_" + (anio - 1).ToString()] = acumAnterior_local.ToString();

                     if (  ValidDate( anio, _mes, _dia) ) {
                        DataOut[ix][el["Local"].ToUpper() + "_" + anio.ToString()] = monto_actual.ToString();
                        DataOut[ix][el["Local"].ToUpper() + "_ACUM_" + anio.ToString()] = acum_local.ToString();
                        DataOut[ix][el["Local"].ToUpper() + "_VAR"] = (acum_local- acumAnterior_local).ToString();
                        DataOut[ix][el["Local"].ToUpper() + "_VARP"] = variacion_procentual(acum_local, acumAnterior_local);
                    }
                    else
                    {
                        DataOut[ix][el["Local"].ToUpper() + "_" + anio.ToString()] = "";
                        DataOut[ix][el["Local"].ToUpper() + "_VAR"] = "";
                        DataOut[ix][el["Local"].ToUpper() + "_VARP"] = "";
                    }

                    /*
                    DataOut[ix][el["Local"].ToUpper() + "_" + (anio - 1).ToString()] = monto_anterior.ToString("C");
                    DataOut[ix][el["Local"].ToUpper() + "_" + anio.ToString()] = monto_actual.ToString("C");
                    DataOut[ix][el["Local"].ToUpper() + "_VAR"] = string.Format( "{0:0.00}" , variacion) + " %";
                    */
                }

            }

            #region Calcula Totales por Fila
            for( int i=1; i<= 31; i++ )
            {
                if (i >0 /* && Totales_year[i] > 0 */ )
                {
                    acumulado += Totales_year[i];
                    // DataOut[i]["TOTALES_" + anio.ToString() ] = Totales_year[i].ToString("C");
                    if (ValidDate(anio, _mes, i))
                    {
                        DataOut[i]["TOTALES_" + anio.ToString()] = Totales_year[i].ToString();
                    } else
                    {
                        DataOut[i]["TOTALES_" + anio.ToString()] = "";
                    }
                }
                if (i > 0 /* && Totales_oldyear[i] > 0 */)
                {
                    acumulado_oldyear += Totales_oldyear[i];
                    // DataOut[i]["TOTALES_" + (anio-1).ToString()] = Totales_oldyear[i].ToString("C");
                    DataOut[i]["TOTALES_" + (anio - 1).ToString()] = Totales_oldyear[i].ToString();
                }
                if (i > 0 && (Totales_year[i] > 0 ||  Totales_oldyear[i] > 0)  )
                {
                    double variacion = 0;

                    if((double)Totales_oldyear[i] == 0 )
                    {
                        variacion = 100;
                    }
                    else
                    {
                        variacion = ((double)Totales_year[i] / (double)Totales_oldyear[i] - 1) * 100;
                    }

                    // DataOut[i]["TOTALES_VAR"] = string.Format("{0:0.00}", variacion) + " %";
                    if (ValidDate(anio, _mes, i))
                    {
                        DataOut[i]["TOTALES_VAR"] = (Totales_year[i] -Totales_oldyear[i]).ToString() ;
                        DataOut[i]["TOTALES_VARP"] = string.Format("{0:0.00}", variacion);
                    }
                    else
                    {
                        DataOut[i]["TOTALES_VAR"] = "";
                        DataOut[i]["TOTALES_VARP"] = "";
                    }




                    DataOut[i]["ACUMULADO_" + (anio - 1).ToString()] = acumulado_oldyear.ToString();

                    if ( ValidDate(anio, _mes, i))
                    {
                        double variacion2 = 0;

                        if ((double)acumulado_oldyear == 0)
                        {
                            variacion2 = 100;
                        }
                        else
                        {
                            variacion2 = ((double)acumulado / (double)acumulado_oldyear - 1) * 100;
                        }


                        DataOut[i]["ACUMULADO_" + anio.ToString()] = acumulado.ToString();
                        DataOut[i]["ACUMULADO_VAR"] = (acumulado - acumulado_oldyear).ToString();
                        DataOut[i]["ACUMULADO_VARP"] = string.Format("{0:0.00}", variacion2);
                    }
                    else
                    {
                        DataOut[i]["ACUMULADO_" + anio.ToString()] = "";
                        DataOut[i]["ACUMULADO_VAR"] = "" ;
                        DataOut[i]["ACUMULADO_VARP"] = "";
                    }


                }
              
            }
            #endregion


            #region Calcula Totales Globales



            #endregion

            return DataOut;
        }


        private bool ValidDate( int anio, int mes, int dia )
        {
            try
            {
                var dt = new DateTime(anio, mes, dia);
                return true;
                // return (new DateTime(anio, mes, dia)) <= DateTime.Today;

            }
            catch ( Exception e) { }

            return false;
        }

        private List<Dictionary<string, string>> Totalizar(int anio, IEnumerable<Dictionary<string, string>> Data_In)
        {
            if( Data_In.Count() <=0 )
            {
                return (List<Dictionary<string, string>>)Data_In;
            }

            // Inicializa Fila
            var row0 = Data_In.FirstOrDefault();
            Dictionary<string, string> row_totales = new Dictionary<string, string>();
            foreach (var c in row0)
            {
                row_totales[c.Key] = "";
            }
            row_totales["dia"] = "TOTALES";


            // Sumas
            foreach ( var row in Data_In.Skip(1).ToList() )
            {
                foreach (var c in row)
                {
                    try
                    {
                        row_totales[c.Key] = Acumular(row_totales[c.Key], c.Key, c.Value);
                    }
                    catch ( Exception e) {
                        string a = e.ToString();
                    }
                }
            }




            // Calcular Variaciones
            Dictionary<string, string> vars = new Dictionary<string, string>();

            var variaciones = row_totales.Where(p => p.Key.Contains("_VARP") && !p.Key.Contains("ACUMULADO")).Select(p => p.Key.Substring(0, p.Key.Length - 5));
            foreach (var variacion in variaciones)
            {
                long v0 = 0;
                long v = 0;
                try { long.TryParse(row_totales[variacion + "_" + anio.ToString()], out v0); } catch (Exception e) { }
                try { long.TryParse(row_totales[variacion + "_" + (anio - 1).ToString()], out v); } catch (Exception e) { }

                try
                {


                    vars[variacion + "_VAR"] = (v0 - v).ToString();
                    vars[variacion + "_VARP"] = variacion_procentual(row_totales[variacion + "_" + anio.ToString()], row_totales[variacion + "_" + (anio - 1).ToString()]);
                }
                catch (Exception e)
                {
                    string errr = e.ToString();
                }
            }

            foreach (var v in vars)
            {
                row_totales[v.Key] = v.Value;
            }


            // Prepare response
            List<Dictionary<string, string>> DataOut = (List<Dictionary<string, string>>)  Data_In;

            DataOut.Add( row_totales );

            return DataOut;
        }

        private string variacion_procentual(double _now , double _last  )
        {
            if( _last !=0 )
            {
                var v = ( _now / _last  - 1) * 100;
                return string.Format("{0:0.00}", v);
            }  else {
                if( _now != 0 )
                {
                    return "100.00"; 
                }

            }
            return "";
        }

        private string variacion_procentual(string now, string last)
        {
            double _now = 0; double _last = 0;

            try { double.TryParse(now, out _now); } catch (Exception e) { }
            try { double.TryParse(last, out _last); } catch (Exception e) { }

            return variacion_procentual(_now, _last);
        }

        private string variacion_procentual(long now, long last)
        {
            return variacion_procentual( (double)now, (double)last);
        }


        private string variacion_procentual(int now, int last)
        {
            return variacion_procentual((double)now, (double)last);
        }


        private string Acumular( string s, string key, string valor)
        {
            if(key.Contains("_") && !key.Contains("_VAR") && !key.Contains("ACUMULADO_"))
            {
                long totalvalue = 0; long value = 0;

                try { long.TryParse(s, out totalvalue); }   catch (Exception e) { }
                try { long.TryParse(valor, out value); } catch (Exception e) { }
                
                

                return (totalvalue + value).ToString();
            }
            return "";
        }


        public void Dispose()
        {
        }
    }
}
