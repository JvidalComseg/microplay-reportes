﻿using Infraestructura.Procedimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicio
{
    public class Menu
    {
        private RSPSample repositorio;

        public Menu()
        {
            repositorio = new RSPSample();
        }

        public IEnumerable<Dictionary<string, string>> GetMenu(string UserName, string Empresa )
        {
            try
            {
                string sp_name_user = "SP_REPORTES_MENU_X_USUARIO";

                Dictionary<string, Object> Parameters = new Dictionary<string, object>();
                Parameters.Add("@usuario", UserName);
                Parameters.Add("@empresa", Empresa);

                return repositorio.Obtener(sp_name_user, Parameters).ToList();
            }
            catch (Exception ex)
            {
            }

            return null;

        }


        public string GetPageTitle(string Url)
        {
            try
            {
                string sp_name_user = "SP_REPORTES_MENU_TITULO_X_URL";

                Dictionary<string, Object> Parameters = new Dictionary<string, object>();
                Parameters.Add("@url", Url);

                var data_menu = repositorio.Obtener(sp_name_user, Parameters).ToList();
                if( data_menu != null && data_menu.Count() > 0 )
                {
                    return data_menu[0]["DETALLE"]; 
                }


            }
            catch (Exception ex)
            {
            }

            return string.Empty;

        }



    }
}
