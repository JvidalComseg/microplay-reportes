﻿using Infraestructura.Procedimientos;
using Servicio.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicio
{
    public class Orders : IDisposable
    {
        private RSPSample repositorio;

        public Orders()
        {
            repositorio = new RSPSample();
        }


        #region Documento
        public Dictionary<string, string> Documento(string TipoDocto, string Numero)
        {
            string sp_name = "SP_INFORME_DETALLE_DOCUMENTO";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();
            Parameters.Add("TipoDocto", TipoDocto);
            Parameters.Add("Numero", Numero);

            Dictionary<string, string> ret = new Dictionary<string, string>();

            var t = repositorio.Obtener(sp_name, Parameters);

            var t0 = t.FirstOrDefault();

            return t0;
        }

        public IEnumerable<Dictionary<string, string>>  DocumentoD(string TipoDocto, string Numero)
        {
            string sp_name = "SP_INFORME_DETALLE_DOCUMENTOD";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();
            Parameters.Add("TipoDocto", TipoDocto);
            Parameters.Add("Numero", Numero);

            return repositorio.Obtener(sp_name, Parameters);
        }

        public IEnumerable<Dictionary<string, string>> DocumentoP(string TipoDocto, string Numero)
        {
            string sp_name = "SP_INFORME_DETALLE_DOCUMENTOP";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();
            Parameters.Add("TipoDocto", TipoDocto);
            Parameters.Add("Numero", Numero);

            return repositorio.Obtener(sp_name, Parameters);
        }


        #endregion


        public void Dispose()
        {
        }
    }
}
