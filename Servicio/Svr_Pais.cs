﻿using Servicio.Interfaces;
using System;
using System.Collections.Generic;
using Dominio.Entidades;
using Servicio.Validaciones;
using Infraestructura.Interfaces;
using Infraestructura.Repositorio;

namespace Servicio
{
    public class Svr_Pais : IServicio<Pais>
    {
        private IValidationDictionary _validationDictionary;
        private IRepositorio<Pais> repositorio;

        public Svr_Pais(IValidationDictionary validationDictionary)
            : this(validationDictionary, new RPais())
        { }

        public Svr_Pais(IValidationDictionary validationDictionary,
                           IRepositorio<Pais> repositorio)
        {
            _validationDictionary = validationDictionary;
            this.repositorio = repositorio;
        }

        public bool EsEntidadValidad(Pais entidad)
        {
            //Agregue aquí toda la validación que desee hacer
            return _validationDictionary.IsValid;
        }


        #region PaisServicio miembros

        public bool Registrar(Pais entidad)
        {
            if (!EsEntidadValidad(entidad))
                return false;
            try
            {
                repositorio.Registrar(entidad);
            }
            catch { return false; }
            return true;
        }

        public bool Eliminar(int id)
        {
            try
            {
                Pais entidad = repositorio.Obtener(id);
                repositorio.Eliminar(entidad);
            }
            catch { return false; }
            return true;
        }

        public bool Actualizar(Pais entidad)
        {
            if (!EsEntidadValidad(entidad))
                return false;
            try
            {
                repositorio.Actualizar(entidad);
            }
            catch (Exception ex)
            {

                return false;
            }
            return true;
        }

        public Pais Obtener(int id)
        {
            return repositorio.Obtener(id);
        }

        public ICollection<Pais> Obtener()
        {
            return repositorio.Obtener();
        }

        #endregion

    }
}
