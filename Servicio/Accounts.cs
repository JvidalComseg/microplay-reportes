﻿using Infraestructura.Procedimientos;
using Servicio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicio
{
    public class Accounts
    {
        private RSPSample repositorio;

        public Accounts()
        {
            repositorio = new RSPSample();
        }


        private string[] ObtenerRoles( string Username)
        {
            List<string> roles = new List<string>();

            string sp_name = "SP_LOGIN_REPORTES_ROLES";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("@usuario", Username);

            var data = repositorio.Obtener(sp_name, Parameters).ToList();
            if (data != null && data.Count() > 0 )
            {
                foreach( var row in data )
                {
                    roles.Add(row["ROL"]);
                }
            }
            return roles.ToArray() ;
        }

        public User LoginByUsernamePassword( string Username, string Password )
        {
            try
            {
                string sp_name = "SP_LOGIN_REPORTES";

                Dictionary<string, Object> Parameters = new Dictionary<string, object>();

                Parameters.Add("@usuario", Username);
                Parameters.Add("@contrasena", Password);

                var data = repositorio.Obtener(sp_name, Parameters).ToList() ;

                if( data != null && data.Count() == 1 )
                {
                    return (new User() {
                        Username = data[0]["USUARIO"],
                        Name = data[0]["NOMBRES"],
                        LastName = data[0]["APELLIDOS"],
                        Roles = ObtenerRoles(data[0]["USUARIO"]) ,
                        FlexUserName = data[0]["FLEXUSERNAME"]

                    } );
                }

            }
            catch (Exception ex)
            {
            }
            return null;
        }



    }
}
