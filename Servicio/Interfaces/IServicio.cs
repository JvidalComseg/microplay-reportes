﻿using System.Collections.Generic;


namespace Servicio.Interfaces
{
    public interface IServicio<T> where T : class
    {
        bool Registrar(T entidad);
        bool Eliminar(int id);
        bool Actualizar(T entidad);
        T Obtener(int id);
        ICollection<T> Obtener();

    }
}
