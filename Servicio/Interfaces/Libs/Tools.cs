﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicio.Libs
{
    public static class Tools
    {
        public static IEnumerable<Dictionary<string, string>> ClearResults( string search, IEnumerable<Dictionary<string, string>> DataIn)
        {
            List<Dictionary<string, string>> DataOut = new List<Dictionary<string, string>>();

            foreach (var row in DataIn)
            {
                Dictionary<string, string> orow = new Dictionary<string, string>();
                foreach (var d in row)
                {
                    string Key = d.Key.Replace(search, "");
                    orow[Key] = d.Value.Replace(search, "");
                }
                DataOut.Add(orow);
            }
            return DataOut;
        }
    }
}
