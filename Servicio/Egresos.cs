﻿using Infraestructura.Procedimientos;
using Servicio.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicio
{
    public class Egresos : IDisposable
    {
        private RSPSample repositorio;

        public Egresos()
        {
            repositorio = new RSPSample();
        }


        #region Bodegas
        public Dictionary<string, string> Bodegas(string empresa)
        {
            string sp_name = "SP_EGRESOS_BODEGAS";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();
            Parameters.Add("empresa", empresa);


            var items = repositorio.Obtener(sp_name, Parameters);

            Dictionary<string, string> ret = new Dictionary<string, string>();

            foreach (var item in items)
            {
                ret[item["Bodega"]] = item["Bodega"];
            }

            return ret;
        }
        #endregion



        #region Ingresos por Venta

        public IEnumerable<Dictionary<string, string>> Ventas(string empresa, int ano, string mes, string dia, string sucursal, string pago, string canal)
        {
            if (string.IsNullOrEmpty(pago)) pago = "";
            if (string.IsNullOrEmpty(canal)) canal = "";
            if (string.IsNullOrEmpty(sucursal)) sucursal = "";
            if (string.IsNullOrEmpty(dia)) dia = "";

            string sp_name = "SP_INGRESOS_VENTAS";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();
            
            Parameters.Add("empresa", empresa);
            Parameters.Add("anio", ano);
            Parameters.Add("mes", mes);
            Parameters.Add("dia", dia);
            Parameters.Add("sucursal", sucursal);
            Parameters.Add("codigopago", pago);
            Parameters.Add("canal", canal);

            return repositorio.Obtener(sp_name, Parameters);
        }


        #endregion

        #region Control Ingresos por Venta

        public IEnumerable<Dictionary<string, string>> Compras_Control(string empresa, int ano, string mes, string dia, string sucursal, string bodega)
        {
            if (string.IsNullOrEmpty(bodega)) bodega = "";
            if (string.IsNullOrEmpty(sucursal)) sucursal = "";
            if (string.IsNullOrEmpty(dia)) dia = "";

            string sp_name = "SP_EGRESOS_COMPRAS_CONTROL";

            Dictionary<string, Object> Parameters = new Dictionary<string, object>();

            Parameters.Add("empresa", empresa);
            Parameters.Add("anio", ano);
            Parameters.Add("mes", mes);
            Parameters.Add("dia", dia);
            Parameters.Add("sucursal", sucursal);
            Parameters.Add("bodega", bodega);

            return repositorio.Obtener(sp_name, Parameters);
        }


        #endregion


        public void Dispose()
        {
        }
    }
}
