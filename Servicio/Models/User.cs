﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicio.Models
{
    public class User
    {
        public string Username
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
        public string LastName
        {
            get;
            set;
        }

        public string FlexUserName
        {
            get;
            set;
        }


        public string[] Roles { get; set; }

    }
}
