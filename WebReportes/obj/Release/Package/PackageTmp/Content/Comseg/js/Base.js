﻿//Quitar espacios en blanco
function lTrim(sStr) {
    if (typeof sStr === 'undefined') return '';
        while (sStr.charAt(0) === " ")
            sStr = sStr.substr(1, sStr.length - 1);
        return sStr;
    }

    function rTrim(sStr) {
        if (typeof sStr === 'undefined') return '';
        while (sStr.charAt(sStr.length - 1) === " ")
            sStr = sStr.substr(0, sStr.length - 1);
        return sStr;
    }

    function allTrim(sStr) { return rTrim(lTrim(sStr)); }

//Dar formato a numero
    function formatNumber(num, prefix) {
        prefix = prefix || '';
        num += '';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
        var regx = /(\d+)(\d{3})/;

        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + '.' + '$2');
        }

        return prefix + splitLeft + splitRight;
    }

    function unformatNumber(num) {
        return num.replace(/([^0-9\,\-])/g, '') ;
    }


function fmt_number(v) {
    return formatNumber(v, "");
}

function fmt_percent(v) {
    v = allTrim(v);
    if (v === "")
        return "";
    return v + "%";
}

function fmt_currency(v) {
    return formatNumber(v, "$");
}


function str_to_number(s) {
    // console.log( 'str_to_number: (' + s + ')'  );
    var ss = allTrim(s);
    // console.log( '>' + ss + '<' );
    ss = unformatNumber(ss);
    // console.log(ss);
    return parseFloat( ss );  
}


function fmt_fecha(anio, mes, dia)
{
    anio = allTrim('' + anio); 
    mes = allTrim('' + mes); 
    dia = allTrim('' + dia); 


    var fecha = '';

    if (typeof anio !== 'undefined' && anio != '' && anio != 'undefined')
        fecha = fecha + allTrim('' + anio);

    if (typeof mes !== 'undefined' && mes != '' && mes != 'undefined') {
        if (fecha != '') fecha = fecha + '/';
        fecha = fecha + allTrim(mes) ;
    }

    if (typeof dia !== 'undefined' && dia != '' && dia != 'undefined') {
        if (fecha != '') fecha = fecha + '/';
        fecha = fecha + allTrim(dia);
    }

    return fecha; 
}
