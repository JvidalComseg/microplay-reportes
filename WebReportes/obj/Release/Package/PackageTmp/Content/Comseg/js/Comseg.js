﻿$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '< Ant',
    nextText: 'Sig >',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    inline: true
};
$.datepicker.setDefaults($.datepicker.regional['es']);




function form_default_apply() {
    
    $(".chosen-select").val('').trigger("chosen:updated");
    $(".chosen-select-multiple").val('').trigger("chosen:updated");
    
    $("form input[default]").each(function () { $(this).val($(this).attr("default"));}); 
}

function DDLResumeTo() {
    $("select[resumeto]").each(function () {
        var select = $(this);
        var data = $("option:selected", select).map(function () { return $(this).val(); }).get().join(','); 
        $( select.attr("resumeto") ).val( data);
    });
}

function ActiveMenuByUrl(url) {
    console.log( url );
}


function DoResize() {

}



$(document).ready(function () {

    $(".ECommerce select").change(function () {

        sel = $(this).val();
        url = $(this).data('url');

        console.log(  url + '  ::  ' +  sel );
        $.ajax({
            type: "GET",
            url: url ,
            data: 'empresa=' + sel,
            success: function (response) {
               location.href = '/';
            }
        });

    });


    var currentDate = new Date();

    $(".complete_vendedor" ).autocomplete({
            source: "/Api/Locales/Vendedores",
            minLength: 3,
            select: function (event, ui) {
                console.log("Selected: " + ui.item.value + " aka " + ui.item.id);
            }
    });

    $(".date_control").datepicker();
    $(".date_control").datepicker( "setDate", currentDate );



    $(window).resize(function () {
        DoResize();
    });

    $(".toggle_fullscreen").click(function (e) {
        e.preventDefault();

        $("body").toggleClass("fullscreen");
    });

    form_default_apply();

    if (typeof app_current_uri !== "undefined") {
        ActiveMenuByUrl(app_current_uri);
    }

    $(".chosen-select") .each(function () { $(this).attr("data-placeholder", $(this).attr("placeholder")); }).chosen();
    $(".chosen-select-multiple").attr("data-placeholder", "Seleccionar sucursal(es) ...").chosen();

    $(".form_clear").click(function (e) {
        e.preventDefault();

        data_form = $(this).data("form");
        data_response = $($(this).data("response"));

        form = $("form[name='" + data_form + "']")[0];

        form.reset();
        form_default_apply(); 
        data_response.html("");
    });

    $(".form_export").click(function (e) {
        e.preventDefault();

        DDLResumeTo();

        data_response = $($(this).data("response"));
        data_response.html("<p class='loading'> Buscando información... </p>");

        data_form = $(this).data("form");
        form = $("form[name='" + data_form + "']")[0];

        $("input[name=descarga]", $(form)).val("true");

        $(".form_enviar").trigger("click");
 
    });  



    $(".form_submit").click(function (e) {
        e.preventDefault();

        DDLResumeTo();

        data_form = $(this).data("form"); 
        data_response = $($(this).data("response")); 

        form = $("form[name='" + data_form + "']");

        data_response.html("<p class='loading'>  Buscando información... </p>");

        console.log(form.serialize());

        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {

                data_response.html(response);

                opciones = {
                    ancho: 0,
                    render: 'fnRender',  /* Funcion que pinta Resumen en hover event*/
                    toolbar: {
                        fullscreen: { label: 'Fullscreen', trigger: '' },
                        export: { label: 'exportar' , trigger: '' }
                    }
                };

                $(".table-resumida").TableResume(opciones);


                $("a.documento").unbind().click(function (e) {
                    e.preventDefault();

                    $.colorbox({
                        width: "80%",
                        height: "90%",
                        iframe: true,
                        href: $(this).attr('href'),
                        opacity: 0.6
                    });


                });



            }
        });

    });  


    $(".Number").each(function () {
        $(this).html(fmt_number($(this).html()));
    });

});