﻿using Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebReportes.Controllers;
using WebReportes.Utils;

namespace WebReportes.Areas.Api.Controllers
{


    public class Vendedor
    {
        public string id { get; set; }
        public string label { get; set; }
        public string value { get; set; }
    }


    [Authorize]
    public class LocalesController : Controller
    {

        [HttpGet]
        public JsonResult Vendedores( string term)
        {
            string Empresa = SessionEx.Get(SessionEx.eCommerce);

            List<Vendedor> ret = new List<Vendedor>();

            using (RrHh ventas = new RrHh())
            {
                var data = ventas.Vendedores(Empresa, term);
                foreach (var row in data)
                {
                    ret.Add(new Vendedor() { id = row["id"], label = row["label"], value = row["value"] });
                }
            }
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

    }
}