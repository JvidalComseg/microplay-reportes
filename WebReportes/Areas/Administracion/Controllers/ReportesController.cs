﻿using Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebReportes.Helpers;

namespace WebReportes.Areas.Administracion.Controllers
{
    [Authorize]
    public class ReportesController : Controller
    {

        [HttpGet]
        [AuthorizeByRol]
        [MenuFilter]
        // GET: Administracion/Reportes
        public ActionResult Index()
        {
            Admin adm = new Admin();
            var usuarios = adm.Reportes_Listado();

            return View();
        }


    }
}