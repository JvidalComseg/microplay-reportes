﻿using Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebReportes.Helpers;

namespace WebReportes.Areas.Administracion.Controllers
{
    [Authorize]
    public class RolesController : Controller
    {

        [HttpGet]
        [AuthorizeByRol]
        [MenuFilter]
        // GET: Administracion/Roles
        public ActionResult Index()
        {
            Admin adm = new Admin();
            var usuarios = adm.Roles_Listado();

            return View();
        }
    }
}