﻿using Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebReportes.Utils;

namespace WebReportes.Helpers
{
    public class MenuFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            string Url     = filterContext.HttpContext.Request.RawUrl;
            string Usuario = SessionEx.Get(SessionEx.UserName);
            string Empresa = SessionEx.Get(SessionEx.eCommerce);

            Menu menu = new Menu();

            //  get the view bag
            var viewBag = filterContext.Controller.ViewBag;

            // set the viewbag values
            viewBag.Url = Url;
            viewBag.Usuario = Usuario;
            viewBag.Menu = menu.GetMenu(Usuario, Empresa);
            viewBag.Title = menu.GetPageTitle(Url);

            base.OnActionExecuting(filterContext);

        }
    }
}


