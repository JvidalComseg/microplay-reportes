﻿using Servicio;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;
using WebReportes.Utils;

namespace WebReportes.Helpers
{
    public static class User_Helper
    {


        public static string UserName()
        {
            return SessionEx.Get(SessionEx.UserName);
        }


        public static string ECommerce()
        {
            return SessionEx.Get(SessionEx.eCommerce);

        }

        public static string ECommerceName()
        {
            return SessionEx.Get(SessionEx.eCommerceName);

        }

        public static string FlexUserName()
        {
            return SessionEx.Get( SessionEx.FlexUserName);
        }


        public static IHtmlString Select_ECommerce( string url )
        {
            string usuario = SessionEx.Get(SessionEx.UserName);


            Common common = new Common();
            var ecoms = common.ECommerce(usuario); 

            if (ecoms.Count == 0)
            {

            } else if( ecoms.Count == 1)
            {
                foreach (KeyValuePair<string, string> item in ecoms)
                {
                    string ss_ecom = SessionEx.Get(SessionEx.eCommerce);
                    if ( string.IsNullOrEmpty(ss_ecom))
                    {
                        SessionEx.Set(SessionEx.eCommerce, item.Key);
                        return new HtmlString("<script>  window.location.href = window.location.href ; </script>");

                    }

                }
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                string curEcom = SessionEx.Get(SessionEx.eCommerce);
                sb.Append("<select data-url='" + url + "'>");
                sb.Append("<option value='' > Seleccionar Empresa ...</option>");
                foreach (KeyValuePair<string, string> item in ecoms)
                {
                    if (curEcom.Equals(item.Key))
                    {
                        sb.Append("<option value='" + item.Key + "' selected >" + item.Value + "</option>");
                    }
                    else
                    {
                        sb.Append("<option value='" + item.Key + "'>" + item.Value + "</option>");
                    }

                }
                sb.Append("</select>");
                return new HtmlString(sb.ToString());
            }

            return new HtmlString(string.Empty);
        }



        public static IHtmlString FullName()
        {

            string data = SessionEx.Get( SessionEx.FullName);
            return new HtmlString( CultureInfo.InvariantCulture.TextInfo.ToTitleCase( data.ToLower() ) );

        }

        public static IHtmlString UserRoles()
        {
            string data = SessionEx.Get( SessionEx.Roles);
            return new HtmlString(CultureInfo.InvariantCulture.TextInfo.ToTitleCase(data.ToLower()));
           
        }



        public static IHtmlString CurrentUri()
        {
            try
            {
                var request = HttpContext.Current.Request; 
                return new HtmlString("/" + VirtualPathUtility.MakeRelative("~", request.Url.AbsolutePath));
            }
            catch (Exception ex) { }
            return new HtmlString("");
        }


        public static IHtmlString Documento(string TipoDocto, string Numero)
        {
            try
            {
                string destino = "/documento/detalle?TipoDocto=" + TipoDocto + "&Numero=" + Numero;
                string link = "<a class='documento' href='" + destino + "'>" + Numero  + "</a>";
                var request = HttpContext.Current.Request;
                return new HtmlString( link);
            }
            catch (Exception ex) { }
            return new HtmlString("");
        }



    }
}