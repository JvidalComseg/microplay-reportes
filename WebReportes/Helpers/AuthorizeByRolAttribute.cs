﻿using Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using WebReportes.Utils;

namespace WebReportes.Helpers
{
    public class AuthorizeByRolAttribute : AuthorizeAttribute
    {

        private string getMethodByPage( string UrlPAge )
        {
            Common commons = new Common();

            return commons.Roles_Pagina(UrlPAge);
        }


        private bool CrossCheckRoles(string UserRoles, string UrlPAge)
        {
            string[] _UserRoles = UserRoles.ToLower().Split(',');

            
            if (_UserRoles.Contains("developer") )
                return true;

            string MethodRoles = this.getMethodByPage( UrlPAge );

            string[] _MethodRoles = MethodRoles.Split(',');

            foreach( var UserRol in _UserRoles )
            {
                if(_MethodRoles.Contains(UserRol.ToLower()))
                    return true; 
            }

            return false;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var empresa = SessionEx.Get(SessionEx.eCommerce);


            var page = httpContext.Request.Url.AbsolutePath.ToLower() ;

            //  page = page.Substring(0, page.Length - 4)  +  "/" ;

            string UserRoles = SessionEx.Get( SessionEx.Roles );

            return CrossCheckRoles(UserRoles, page) ; 
        }


        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var url = filterContext.HttpContext.Request.Url;

            RedirectToRouteResult routeData = null;

                routeData = new RedirectToRouteResult
                (new System.Web.Routing.RouteValueDictionary
                 (new
                 {
                     controller = "Error",
                     action = "AccessDenied",
                     RedirectUrl = url
                 }
                 ));

            filterContext.Result = routeData;
        }

    }
}