﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebReportes.Startup))]
namespace WebReportes
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
