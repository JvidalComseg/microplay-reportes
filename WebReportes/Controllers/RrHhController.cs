﻿using Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebReportes.Helpers;
using WebReportes.Utils;

namespace WebReportes.Controllers
{


    [Authorize]
    public class RrHhController : Controller
    {
        #region Comisiones de Venta

        [HttpGet]
        [AuthorizeByRol]
        [MenuFilter]
        public ActionResult Comisiones()
        {
            string Empresa = SessionEx.Get(SessionEx.eCommerce);

            Common commons = new Common();

            var sucursales = commons.SucursalesXEmpresa(Empresa);
            ViewBag.sucursales = sucursales.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();
            ViewBag.anio = commons.ListaAnios();
            ViewBag.meses = commons.ListaMeses();

            return View();
        }


        [HttpPost]
        [AuthorizeByRol]
        public ActionResult Comisiones(FormCollection form)
        {
            try
            {
                string Empresa = SessionEx.Get(SessionEx.eCommerce);
                int anio = 0;
                string mes = "";
                string sucursales = "";
                bool descarga = false;

                int.TryParse(form["anio"], out anio);
                mes = form["mes"];
                sucursales = form["sucursales"];
                bool.TryParse(form["descarga"], out descarga);

                using (RrHh ventas = new RrHh())
                {
                    var data = ventas.ComisionesLocal(Empresa, anio, mes, sucursales);

                    if (descarga)
                    {
                        this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                        this.Response.AddHeader("content-disposition", string.Format("inline;filename=Comisiones_Venta_{0}-{1}-{2}.xls", Empresa, anio, mes));
                    }

                    return PartialView("Comisiones_Resultado", data);
                }


            }
            catch (Exception e)
            {

            }
            return PartialView("Error");
        }


        #endregion


        #region Ventas Locales

        [HttpGet]
        [AuthorizeByRol]
        [MenuFilter]
        public ActionResult VentasLocales()
        {
            string Empresa = SessionEx.Get(SessionEx.eCommerce);
            Common commons = new Common();

            var sucursales = commons.SucursalesXEmpresa(Empresa);
            ViewBag.sucursales = sucursales.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();

            ViewBag.anio = commons.ListaAnios();
            ViewBag.meses = commons.ListaMeses();


            return View();
        }

        [HttpPost]
        [AuthorizeByRol]
        public ActionResult VentasLocales(FormCollection form)
        {
            try
            {
                string Empresa = SessionEx.Get(SessionEx.eCommerce);
                string sucursales = "";
                string anio = "";
                string mes = "";
                bool descarga = false;

                sucursales = form["sucursales"];

                anio = form["anio"];
                int iAnio = 0;
                int.TryParse(anio, out iAnio);

                mes = form["mes"];

                bool.TryParse(form["descarga"], out descarga);

                using (RrHh ventas = new RrHh())
                {
                    var data = ventas.VentasLocales(Empresa, sucursales, iAnio, mes);

                    if (descarga)
                    {
                        sucursales = sucursales.Replace(',', '-');

                        this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                        
                        this.Response.AddHeader("content-disposition", string.Format("inline;filename=Ventas_Sucursales_{0}-{1}_{2}-{3}.xls", Empresa, sucursales, anio, mes));
                    }

                    return PartialView("VentasLocales_Resultado", data);
                }


            }
            catch (Exception e)
            {

            }
            return PartialView("Error");
        }


        #endregion


        #region Ventas Vendedor

        [HttpGet]
        [AuthorizeByRol]
        [MenuFilter]
        public ActionResult VentasVendedor()
        {
            return View();
        }

        [HttpPost]
        [AuthorizeByRol]
        public ActionResult VentasVendedor(FormCollection form)
            {
                try
                {
                    string vendedor = "";
                    string fechaInicio = "";
                    string fechaFinal = "";
                    bool descarga = false;

                    vendedor = form["vendedor"];
                    fechaInicio = form["fechaInicio"];
                    fechaFinal = form["fechaFinal"];

                    bool.TryParse(form["descarga"], out descarga);

                    using (RrHh ventas = new RrHh())
                    {
                        var data = ventas.VentaVendedor(vendedor,  fechaInicio,  fechaFinal);

                        this.ViewBag.Resumen = ventas.VentaVendedorResumen(vendedor, fechaInicio, fechaFinal);

                        if (descarga)
                        {
                            this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                            this.Response.AddHeader("content-disposition", string.Format("inline;filename=Ventas_Vendedor_{0}_{1}-{2}.xls", vendedor, fechaInicio, fechaFinal));
                        }

                        return PartialView("VentasVendedor_Resultado", data);
                    }


                }
                catch (Exception e)
                {

                }
                return PartialView("Error");
            }



        [HttpGet]
        [AuthorizeByRol]
        [MenuFilter]
        public ActionResult VentasVendedorGE()
        {
            return View();
        }


        [HttpPost]
        [AuthorizeByRol]
        public ActionResult VentasVendedorGE(FormCollection form)
        {
            try
            {
                string vendedor = "";
                string fechaInicio = "";
                string fechaFinal = "";
                bool descarga = false;

                vendedor = form["vendedor"];
                fechaInicio = form["fechaInicio"];
                fechaFinal = form["fechaFinal"];

                bool.TryParse(form["descarga"], out descarga);

                using (RrHh ventas = new RrHh())
                {
                    var data = ventas.VentaVendedorGE(vendedor, fechaInicio, fechaFinal);

                    this.ViewBag.Resumen = ventas.VentaVendedorGEResumen(vendedor, fechaInicio, fechaFinal);

                    if (descarga)
                    {
                        this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                        this.Response.AddHeader("content-disposition", string.Format("inline;filename=Ventas_Vendedor_GE_{0}_{1}-{2}.xls", vendedor, fechaInicio, fechaFinal));
                    }

                    return PartialView("VentasVendedor_Resultado", data);
                }


            }
            catch (Exception e)
            {

            }
            return PartialView("Error");
        }


        #endregion




    }

}