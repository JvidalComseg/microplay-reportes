﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebReportes.Helpers;

namespace WebReportes.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        [MenuFilter]
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Title = "Inicio";
            return View();
        }
    }
}