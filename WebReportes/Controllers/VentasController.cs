﻿using Servicio;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using System;
using WebReportes.Helpers;
using WebReportes.Utils;

namespace WebReportes.Controllers
{

    [Authorize]
    public class VentasController : Controller
    {


        #region Ventas Local
            [HttpGet]
            [AuthorizeByRol]
            [MenuFilter]
            public ActionResult Local()
            {
                string Empresa = SessionEx.Get(SessionEx.eCommerce);

                Common commons = new Common();

                var sucursales = commons.SucursalesXEmpresa( Empresa );
                ViewBag.sucursales = sucursales.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();

                ViewBag.anio = commons.ListaAnios();
                ViewBag.meses = commons.ListaMeses();

                return View();
            }


            [HttpPost]
            [AuthorizeByRol]
            public ActionResult Local(FormCollection form)
            {
                try
                {
                    string Empresa = SessionEx.Get(SessionEx.eCommerce);
                    int anio = 0;
                    string mes = "";
                    string sucursales = "";
                    bool descarga = false;

                    int.TryParse(form["anio"], out anio);
                    mes = form["mes"];
                    sucursales = form["sucursales"];
                    bool.TryParse(form["descarga"], out descarga);

                    using (Ventas ventas = new Ventas())
                    {
                        var data = ventas.Mes_Local( Empresa, anio, mes, sucursales);

                        if (descarga)
                        {
                            this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                            this.Response.AddHeader("content-disposition", string.Format("inline;filename=Ventas_Local_{0}-{1}-{2}.xls",  Empresa ,anio, mes));
                        }

                        ViewBag.Descarga = descarga ? "Si" : "No";
                        return PartialView("Local_Resultado", data);
                    }


                }
                catch (Exception e)
                {

                }
                return PartialView("Error");
            }

        #endregion


        #region Ventas Local Acumuladas
        [HttpGet]
        [AuthorizeByRol]
        [MenuFilter]
        public ActionResult LocalAcumulada()
        {
            string Empresa = SessionEx.Get(SessionEx.eCommerce);

            Common commons = new Common();

            var sucursales = commons.SucursalesXEmpresa(Empresa);
            ViewBag.sucursales = sucursales.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();

            ViewBag.anio = commons.ListaAnios();
            ViewBag.meses = commons.ListaMeses();

            return View("Local_Acumulado");

            // return View();
        }


        [HttpPost]
        [AuthorizeByRol]
        public ActionResult LocalAcumulada(FormCollection form)
        {
            try
            {
                string Empresa = SessionEx.Get(SessionEx.eCommerce);
                int anio = 0;
                string mes = "";
                string sucursales = "";
                bool descarga = false;

                int.TryParse(form["anio"], out anio);
                mes = form["mes"];
                sucursales = form["sucursales"];
                bool.TryParse(form["descarga"], out descarga);

                using (Ventas ventas = new Ventas())
                {
                    var data = ventas.Mes_Local(Empresa, anio, mes, sucursales);

                    if (descarga)
                    {
                        this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                        this.Response.AddHeader("content-disposition", string.Format("inline;filename=Ventas_Local_Acumulada_{0}-{1}-{2}.xls", Empresa, anio, mes));
                    }

                    ViewBag.Descarga = descarga ? "Si" : "No";
                    return PartialView("Local_Acumulado_Resultado", data);
                }


            }
            catch (Exception e)
            {

            }
            return PartialView("Error");
        }


        #endregion


        #region Ventas Web
        [HttpGet]
        [AuthorizeByRol]
        [MenuFilter]
        public ActionResult Web()
        {
            string Empresa = SessionEx.Get(SessionEx.eCommerce);

            Common commons = new Common();

            var sucursales = commons.SucursalesXEmpresa(Empresa);
            ViewBag.sucursales = sucursales.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();

            ViewBag.anio = commons.ListaAnios();
            ViewBag.meses = commons.ListaMeses();

            return View();
        }


        [HttpPost]
        [AuthorizeByRol]
        public ActionResult Web(FormCollection form)
        {
            try
            {
                string Empresa = SessionEx.Get(SessionEx.eCommerce);
                int anio = 0;
                string mes = "";
                string sucursales = "";
                bool descarga = false;

                int.TryParse(form["anio"], out anio);
                mes = form["mes"];
                sucursales = form["sucursales"];
                bool.TryParse(form["descarga"], out descarga);

                using (Ventas ventas = new Ventas())
                {
                    var data = ventas.Mes_Web(Empresa, anio, mes, sucursales);

                    if (descarga)
                    {
                        this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                        this.Response.AddHeader("content-disposition", string.Format("inline;filename=Ventas_Web_{0}-{1}-{2}.xls", Empresa, anio, mes));
                    }

                    ViewBag.Descarga = descarga ? "Si" : "No";
                    return PartialView("Web_Resultado", data);
                }


            }
            catch (Exception e)
            {

            }
            return PartialView("Error");
        }
        #endregion


     
    
    }
}