﻿using Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebReportes.Helpers;
using WebReportes.Utils;

namespace WebReportes.Controllers
{
    [Authorize]
    public class EgresosController : Controller
    {

        #region Compras Control


        [HttpGet]
        [AuthorizeByRol]
        [MenuFilter]
        public ActionResult Control()
        {
            string Empresa = SessionEx.Get(SessionEx.eCommerce);
            Egresos ingresos = new Egresos();

            var bodegas = ingresos.Bodegas(Empresa);
            ViewBag.bodegas = bodegas.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();


            Common commons = new Common();
            var sucursales = commons.SucursalesXEmpresa(Empresa);

            ViewBag.sucursales = sucursales.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();

            ViewBag.anio = commons.ListaAnios();
            ViewBag.meses = commons.ListaMeses();
            ViewBag.dias = commons.ListaDias();

            return View();
        }


        [HttpPost]
        [AuthorizeByRol]
        public ActionResult Control(FormCollection form)
        {
            try
            {
                string Empresa = SessionEx.Get(SessionEx.eCommerce);
                int anio = 0;
                string mes = "";
                string dia = "";
                string sucursales = "";
                string bodega = "";
                bool descarga = false;

                int.TryParse(form["anio"], out anio);
                mes = form["mes"];
                dia = form["dia"];
                sucursales = form["sucursales"];
                bodega = form["bodega"];

                bool.TryParse(form["descarga"], out descarga);

                using (Egresos ingresos = new Egresos())
                {
                    var data = ingresos.Compras_Control(Empresa, anio, mes, dia, sucursales, bodega);

                    if (descarga)
                    {
                        this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                        this.Response.AddHeader("content-disposition", string.Format("inline;filename=Egresos_Control_{0}-{1}-{2}.xls", Empresa, anio, mes));
                    }

                    return PartialView("Control_Resultado", data);
                }


            }
            catch (Exception e)
            {

            }
            return PartialView("Error");
        }
        #endregion



    }
}