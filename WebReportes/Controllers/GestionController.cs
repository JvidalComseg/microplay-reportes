﻿using Servicio;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using System;
using WebReportes.Helpers;
using WebReportes.Utils;

namespace WebReportes.Controllers
{
    [Authorize]
    public class GestionController : Controller
    {


        #region Detalle Margen por Documento

  
            [HttpGet]
            [AuthorizeByRol]
            [MenuFilter]
            public ActionResult MargenPorDocumento()
            {
                string Empresa = SessionEx.Get(SessionEx.eCommerce);
                Common commons = new Common();
                var sucursales = commons.SucursalesXEmpresa(Empresa);

                ViewBag.sucursales = sucursales.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();

                ViewBag.anio = commons.ListaAnios() ;
                ViewBag.meses = commons.ListaMeses();

                return View();
            }



            [HttpPost]
            [AuthorizeByRol]
            public ActionResult MargenPorDocumento(FormCollection form)
            {
                try
                {
                    string Empresa = SessionEx.Get(SessionEx.eCommerce);
                    int anio = 0;
                    string mes = "";
                    string sucursales = "";
                    bool descarga = false;

                    int.TryParse(form["anio"], out anio);
                    mes = form["mes"];
                    sucursales = form["sucursales"];
                    bool.TryParse(form["descarga"], out descarga);

                    using (Gestion ventas = new Gestion())
                    {
                        var data = ventas.MargenPorDocumento(Empresa, anio, mes, sucursales);

                        if (descarga)
                        {
                            this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                            this.Response.AddHeader("content-disposition", string.Format("inline;filename=Detalle_Margen_Por_Documento_{0}-{1}-{2}.xls", Empresa, anio, mes));
                        }

                        return PartialView("MargenPorDocumento_Resultado", data);
                    }


                }
                catch (Exception e)
                {

                }
                return PartialView("Error");
            }


        #endregion

    }
}