﻿//-----------------------------------------------------------------------    
// <copyright file="AccountController.cs" company="None">  
//   Copyright (c) Allow to distribute this code.    
// </copyright>  
// <author>Asma Khalid</author>  
//-----------------------------------------------------------------------    
namespace WebReportes.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Web;
    using System.Web.Mvc;
    using WebReportes.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Servicio;
    using WebReportes.Helpers;
    using WebReportes.Utils;
    using Microsoft.Owin;


    /// <summary>  
    /// Account controller class.    
    /// </summary>  
    public class AccountController : Controller
    {

        #region Login methods 
        
            /// <summary>  
            /// GET: /Account/Login    
            /// </summary>  
            /// <param name="returnUrl">Return URL parameter</param>  
            /// <returns>Return login view</returns>  
            [AllowAnonymous]
            public ActionResult Login(string returnUrl)
            {
                try
                {
                    // Verification.    
                    if (this.Request.IsAuthenticated)
                    {
                        // Info.    
                        return this.RedirectToLocal(returnUrl);
                    }
                }
                catch (Exception ex)
                {
                    // Info    
                    Console.Write(ex);
                }

                ViewBag.ReturnUrl = returnUrl;
                // Info.    
                return this.View();
            }


            /// <summary>  
            /// POST: /Account/Login    
            /// </summary>  
            /// <param name="model">Model parameter</param>  
            /// <param name="returnUrl">Return URL parameter</param>  
            /// <returns>Return login view</returns>  
            [HttpPost]
            [AllowAnonymous]
            [ValidateAntiForgeryToken]
            public ActionResult Login(LoginViewModel model, string returnUrl)
            {
                try
                {
                    // Verification.    
                    if (ModelState.IsValid)
                    {
                        var logindetails = (new Accounts()).LoginByUsernamePassword(model.Username, model.Password);
                        if (logindetails != null )
                        {
                            this.SignInUser( logindetails.Username, logindetails.Name, logindetails.LastName, logindetails.FlexUserName , logindetails.Roles );

                            return this.RedirectToLocal(returnUrl);
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Nombre de usuario y/o contraseña son inválidos.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Info    
                    Console.Write(ex);
                }

                // If we got this far, something failed, redisplay form    
                return this.View(model);
            }

        #endregion

        #region Log Out method.    
            /// <summary>  
            /// POST: /Account/LogOff    
            /// </summary>  
            /// <returns>Return log off action</returns>  
            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult LogOff()
            {
                try
                {
                    // Setting.    
                    var ctx = Request.GetOwinContext();
                    var authenticationManager = ctx.Authentication;
                    // Sign Out.    
                    authenticationManager.SignOut();
                }
                catch (Exception ex)
                {
                    // Info    
                    throw ex;
                }
                // Info.    
                return this.RedirectToAction("Login", "Account");
            }
        #endregion

        #region Authorized Actions

            [Authorize]
            [MenuFilter]
            public ActionResult Perfil()
            {
                ViewBag.Title = "Datos del Usuario";

                Common commons = new Common();
                ViewBag.info = commons.Info();

                return this.View();
            }

        #endregion

        #region Helpers    

            #region Sign In method.    
            /// <summary>  
            /// Sign In User method.    
            /// </summary>  
            /// <param name="username">Username parameter.</param>  
            /// <param name="isPersistent">Is persistent parameter.</param>  
            private void SignInUser(string username, string GivenName, string Surname, string FlexUserName, string[] Roles)
            {
                // var ctx = (OwinContext) Request.GetOwinContext();
                SessionEx.Register( username, GivenName, Surname, FlexUserName, string.Join(",", Roles));
            }
                #endregion


            #region Redirect to local method.    
            /// <summary>  
            /// Redirect to local method.    
            /// </summary>  
            /// <param name="returnUrl">Return URL parameter.</param>  
            /// <returns>Return redirection action</returns>  
            private ActionResult RedirectToLocal(string returnUrl)
            {
                try
                {
                    // Verification.    
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        // Info.    
                        return this.Redirect(returnUrl);
                    }
                }
                catch (Exception ex)
                {
                    // Info    
                    throw ex;
                }
                // Info.    
                return this.RedirectToAction("Index", "Home");
            }
        #endregion


        #region  EMPRESAS
        [HttpGet]
        public ActionResult eccomerce()
        {
            try
            {
                string usuario = SessionEx.Get(SessionEx.UserName) ;
                string empresa = Request.QueryString["empresa"];
                SessionEx.Set(SessionEx.eCommerce, empresa);

                Common common = new Common();
                var eComms = common.ECommerce(usuario);
                var eComm = eComms[empresa];
                SessionEx.Set(SessionEx.eCommerceName, eComm);
            }
            catch (Exception ex)
            {
            }

            return new EmptyResult();
        }
        #endregion

        #endregion
    }
}