﻿using Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebReportes.Helpers;
using WebReportes.Utils;

namespace WebReportes.Controllers
{

    [Authorize]
    public class LocalesController : Controller
    {


        [HttpGet]
        [AuthorizeByRol]
        [MenuFilter]
        public ActionResult VentasPorVendedor()
        {
            string Empresa = SessionEx.Get(SessionEx.eCommerce);
            Common commons = new Common();

            string vendedor = User_Helper.FlexUserName();

            ViewBag.locales = commons.SucursalesXUsuario(vendedor).Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();

            ViewBag.anio = commons.ListaAnios();
            ViewBag.meses = commons.ListaMeses();
            return View();
        }


        [HttpPost]
        [AuthorizeByRol]
        public ActionResult VentasPorVendedor(FormCollection form)
        {
            try
            {
                string Empresa = SessionEx.Get(SessionEx.eCommerce);
                string vendedor = User_Helper.FlexUserName();

                string local = "";
                int anio = 0;
                string mes = "";
                bool descarga = false;

                int.TryParse(form["anio"], out anio);
                mes = form["mes"];
                bool.TryParse(form["descarga"], out descarga);
                local = form["local"];

                Common commons = new Common();
                var locales = commons.SucursalesXUsuario(vendedor);

                if(  string.IsNullOrEmpty(local) ||  ! locales.ContainsKey(local) )
                {
                    return PartialView("ErrorUsuarioLocal");
                }

                using (Ventas ventas = new Ventas())
                {
                    var data = ventas.VentasLocalPorVendedor(Empresa,anio, mes, local);

                    if (descarga)
                    {
                        this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                        this.Response.AddHeader("content-disposition", string.Format("inline;filename=Ventas_Local_X_Vendedor-{0}_{1}-{2}.xls", local, anio, mes));
                    }

                    ViewBag.Descarga = descarga ? "Si" : "No";
                    return PartialView("VentasPorVendedor_Resultado", data);
                }


            }
            catch (Exception e)
            {

            }
            return PartialView("Error");
        }


        [HttpGet]
        [AuthorizeByRol]
        [MenuFilter]
        public ActionResult Vendedor()
        {

            Common commons = new Common();
            string vendedor = User_Helper.FlexUserName();

            ViewBag.locales = commons.SucursalesXUsuario(vendedor).Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();

            ViewBag.anio = commons.ListaAnios();
            ViewBag.meses = commons.ListaMeses();
            return View();
        }


        [HttpPost]
        [AuthorizeByRol]
        public ActionResult Vendedor(FormCollection form)
        {
            try
            {

                Common commons = new Common();
                var listaMeses = commons.ListaMeses();

                string vendedor = User_Helper.FlexUserName();

                string local = "";
                int anio = 0;
                string mes = "";
                bool descarga = false;

                int.TryParse(form["anio"], out anio);
                mes = form["mes"];
                local = form["local"];

                bool.TryParse(form["descarga"], out descarga);

                using (Ventas ventas = new Ventas())
                {
                    var data = ventas.Vendedor(vendedor, anio, mes);

                    ViewBag.U_Nombre = vendedor;
                    ViewBag.U_Sucursal = local;

                    ViewBag.D_Fecha = DateTime.Now.ToString("dd/MM/yyyy");
                    ViewBag.D_Anio = DateTime.Now.Year.ToString();
                    ViewBag.D_Anio_Anterior = (DateTime.Now.Year - 1).ToString();

                    ViewBag.D_Dia = DateTime.Now.Day.ToString();
                    ViewBag.D_Mes = DateTime.Now.Month.ToString();
                    string cmes = String.Format("{0:D2}", DateTime.Now.Month);

                    var mescx = listaMeses.Where<SelectListItem>(p => p.Value == cmes).Select(p => p.Text).FirstOrDefault();
                    if (mescx != null)
                    {
                        ViewBag.D_MesName = mescx;
                    }



                    ViewBag.S_Anio = anio.ToString();
                    ViewBag.S_Anio_Anterior = (anio - 1).ToString();

                    ViewBag.S_Mes = mes;
                    var mesx = listaMeses.Where<SelectListItem>(p => p.Value == mes).Select(p => p.Text).FirstOrDefault();
                    if(  mesx != null )
                    {
                        ViewBag.S_MesName = mesx;
                    }


                    ViewBag.C_TotalDia_Anterior = "";
                    ViewBag.C_TotalDia = "";
                    ViewBag.C_TotalMes_Anterior = "";
                    ViewBag.C_TotalMes = "";
                    ViewBag.S_TotalMes_Anterior = "";
                    ViewBag.S_TotalMes = "";


                    try { ViewBag.C_TotalDia_Anterior = data["Acumulado Mismo Mes Año Anterior"];  } catch (Exception e ) { }
                    try { ViewBag.C_TotalDia = data["Ventas Hoy"];  } catch (Exception e) { }
                    try { ViewBag.C_TotalMes_Anterior = data["Total Mensual Año Anterior"];  } catch (Exception e) { }
                    try { ViewBag.C_TotalMes = data["Acumulado del Mes"];  } catch (Exception e) { }
                    try { ViewBag.S_TotalMes_Anterior = data["Acumulado Periodo Seleccionado Año Anterior"];  } catch (Exception e) { }
                    try { ViewBag.S_TotalMes = data["Acumulado Periodo Seleccionado"];  } catch (Exception e) { }

                    if (descarga)
                    {
                        this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                        this.Response.AddHeader("content-disposition", string.Format("inline;filename=Ventas_Vendedor_{0}_{1}-{2}.xls", vendedor,  anio, mes));
                    }

                    ViewBag.Descarga = descarga ? "Si" : "No";
                    return PartialView("Vendedor_Resultado", data);
                }


            }
            catch (Exception e)
            {

            }
            return PartialView("Error");
        }

    }
}