﻿using Servicio;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using System;
using WebReportes.Helpers;
using WebReportes.Utils;

namespace WebReportes.Controllers
{
    [Authorize]
    public class IngresosController : Controller
    {


        #region Ventas Contabilidad

            [HttpGet]
            [AuthorizeByRol]
            [MenuFilter]
            public ActionResult Pagos()
            {
                string Empresa = SessionEx.Get(SessionEx.eCommerce);


                Ingresos ingresos = new Ingresos();

                var tiposDePago = ingresos.TiposDePago(Empresa);
                ViewBag.codigopago = tiposDePago.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();

                var canal = ingresos.TiposDeCanales(Empresa);
                ViewBag.canal = canal.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();



                Common commons = new Common();
                var sucursales = commons.SucursalesXEmpresa(Empresa);

                ViewBag.sucursales = sucursales.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();

                ViewBag.anio = commons.ListaAnios() ;
                ViewBag.meses = commons.ListaMeses();

                ViewBag.dias = commons.ListaDias();


                return View();
            }


            [HttpPost]
            [AuthorizeByRol]
            public ActionResult Pagos(FormCollection form)
            {
                try
                {
                    string Empresa = SessionEx.Get(SessionEx.eCommerce);

                    int anio = 0;
                    string mes = "";
                    string dia = "";
                    string sucursales = "";
                    string codigopago = "";
                    string canal = "";
                    bool descarga = false;

                    int.TryParse(form["anio"], out anio);
                    mes = form["mes"];
                    dia = form["dia"];

                    sucursales = form["sucursales"];
                    codigopago = form["pago"];
                    canal = form["canal"];

                    bool.TryParse(form["descarga"], out descarga);

                    using (Ingresos ingresos = new Ingresos())
                    {
                        var data = ingresos.Ventas(Empresa, anio, mes, dia, sucursales, codigopago, canal);

                        if (descarga)
                        {
                            this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                            this.Response.AddHeader("content-disposition", string.Format("inline;filename=Ingresos_Pagos_{0}-{1}-{2}.xls", Empresa, anio, mes));
                        }

                        return PartialView("Pagos_Resultado", data);
                    }


                }
                catch (Exception e)
                {

                }
                return PartialView("Error");
            }


        #endregion



        #region Ventas Control


            [HttpGet]
            [AuthorizeByRol]
            [MenuFilter]
            public ActionResult Control()
            {
                string Empresa = SessionEx.Get(SessionEx.eCommerce);
                Ingresos ingresos = new Ingresos();

                var bodegas = ingresos.Bodegas(Empresa);
                ViewBag.bodegas = bodegas.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();

                var canal = ingresos.TiposDeCanales(Empresa);
                ViewBag.canal = canal.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();



                Common commons = new Common();
                var sucursales = commons.SucursalesXEmpresa(Empresa);

                ViewBag.sucursales = sucursales.Select(i => new SelectListItem() { Value = i.Key, Text = i.Value }).ToList();

                ViewBag.anio = commons.ListaAnios();
                ViewBag.meses = commons.ListaMeses();
                ViewBag.dias = commons.ListaDias();

                return View();
            }


            [HttpPost]
            [AuthorizeByRol]
            public ActionResult Control(FormCollection form)
            {
                try
                {
                    string Empresa = SessionEx.Get(SessionEx.eCommerce);
                    int anio = 0;
                    string mes = "";
                    string dia = "";
                    string sucursales = "";
                    string bodega = "";
                    string canal = "";
                    bool descarga = false;

                    int.TryParse(form["anio"], out anio);
                    mes = form["mes"];
                    dia = form["dia"];
                    sucursales = form["sucursales"];
                    bodega = form["bodega"];
                    canal = form["canal"];

                    bool.TryParse(form["descarga"], out descarga);

                    using (Ingresos ingresos = new Ingresos())
                    {
                        var data = ingresos.Ventas_Control(Empresa, anio, mes, dia, sucursales, bodega, canal);

                        if (descarga)
                        {
                            this.Response.AddHeader("content-type", "application/vnd.ms-excel");
                            this.Response.AddHeader("content-disposition", string.Format("inline;filename=Ingresos_Control_{0}-{1}-{2}.xls", Empresa, anio, mes));
                        }

                        return PartialView("Control_Resultado", data);
                    }


                }
                catch (Exception e)
                {

                }
                return PartialView("Error");
            }


        #endregion


    }
}