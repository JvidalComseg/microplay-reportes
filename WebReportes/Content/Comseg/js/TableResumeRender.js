﻿

function fnRender_Datos( elem ) {
    var datos = {
        anio: 2018,
        mes: '01',
        dia_mes: '',
        sucursal: '',
        dia_sucursal: {},
        total_dia: {},
        participacion_dia: {},
        acumulado_dia: {},
        total_sucursal: {},
        total_mes: {},
        participacion_mensual: {}
    };


    tabla = elem.closest("table");
    row = elem.closest("tr");

    row_totales = $("tr.totales", tabla);


    td = $("td", row).first();
    if (td != null) {
        datos.dia_mes = $(td).html();
    }

    //  Total Mensual
    datos_dia = $('td.col_totales', row_totales);
    datos.total_mes.now = $(datos_dia[0]).html();
    datos.total_mes.past = $(datos_dia[1]).html();
    datos.total_mes.variacion = $(datos_dia[2]).html();
    datos.total_mes.variacionp = $(datos_dia[3]).html();

    //  Totales y Acumulados Diarios
    if (typeof datos.dia_mes !== 'undefined') {

        datos_pointer = $('td.col_totales', row);
        datos.total_dia.now = $(datos_pointer[0]).html();
        datos.total_dia.past = $(datos_pointer[1]).html();
        datos.total_dia.variacion = $(datos_pointer[2]).html();
        datos.total_dia.variacionp = $(datos_pointer[3]).html();

        datos_pointer = $('td.col_acumulado', row);
        datos.acumulado_dia.now = $(datos_pointer[0]).html();
        datos.acumulado_dia.past = $(datos_pointer[1]).html();
        datos.acumulado_dia.variacion = $(datos_pointer[2]).html();
        datos.acumulado_dia.variacionp = $(datos_pointer[3]).html();
    }


    var ids = elem.prop('class').match(/col_([0-9]+)/);
    if (ids != null && typeof ids[1] !== 'undefined') {
        sucursal = $('th.col_' + ids[1], tabla)[0];
        datos.sucursal = $(sucursal).html();

        if (typeof datos.dia_mes !== 'undefined') {

            //  Datos Dia
            datos_dia = $('td.col_' + ids[1], row);
            datos.dia_sucursal.now = $(datos_dia[0]).html();
            datos.dia_sucursal.past = $(datos_dia[1]).html();
            datos.dia_sucursal.variacion = $(datos_dia[2]).html();
            datos.dia_sucursal.variacionp = $(datos_dia[3]).html();

            datos_dia = $('td.col_' + ids[1], row_totales);
            datos.total_sucursal.now = $(datos_dia[0]).html();
            datos.total_sucursal.past = $(datos_dia[1]).html();
            datos.total_sucursal.variacion = $(datos_dia[2]).html();
            datos.total_sucursal.variacionp = $(datos_dia[3]).html();

            //  Participacion Diaria Sucursal
            datos.participacion_dia.now = Percent(datos.dia_sucursal.now, datos.total_dia.now);
            datos.participacion_dia.past = Percent(datos.dia_sucursal.past, datos.total_dia.past);
        }

        //  Participacion Mensual Sucursal
        datos.participacion_mensual.now = Percent(datos.total_sucursal.now, datos.total_mes.now);
        datos.participacion_mensual.past = Percent(datos.total_sucursal.past, datos.total_mes.past);
    }


    return datos;
}

function fnRender(elem) {

    var datos = fnRender_Datos(elem); 

    console.log(datos);

    var b = '';

    fecha = fmt_fecha( datos.anio , datos.mes ,datos.dia_mes ) ;
    b = b + '<table><tr><td>Tiendas</td><td>' + 'videojuegos' + '</td></tr>';
    b = b + '<tr><td>Fecha</td><td>' + fecha + '</td></tr>';
    if (datos.sucursal !== '') {
        b = b + '<tr><td>Sucursal</td><td>' + datos.sucursal + '</td></tr>';
    }
    b = b + '</table>';

    if (datos.dia_mes != '' && datos.dia_mes != 'undefined') {

        if (datos.sucursal !== '') {
            datos.dia_sucursal
            b = b + '<table><tr><td colspan="2"> Vta Diaria Sucursal </td></tr>';
            b = b + '<tr><td> </td><td>' + fecha + '</td></tr>';

        }

        // Datos Finales del Dia
    }
    else {
        if (datos.sucursal !== '') {


        }
    }

    // Datos Totales

    // Poner datos en el contenedor
    tabla = elem.closest("table");
    tabla_with_resumen = tabla.closest(".table-container");
    dest = $(".table-resumen-content", tabla_with_resumen);

    $(dest).html( b );
}




