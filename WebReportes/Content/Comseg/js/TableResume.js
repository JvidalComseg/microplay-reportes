﻿
function fnRender_clear(elem) {

    tabla = elem.closest("table");

    tabla_with_resumen = tabla.closest(".table-container");
    dest = $(".table-resumen-content", tabla_with_resumen);

    $(dest).html('');
}

function Percent(n0, m0) {
    var n = str_to_number(n0);
    var m = str_to_number(m0);
    if (m != 0) {
        var x = n / m * 100;
        return fmt_percent(x.toFixed(2));
    }
    return "";
}



(function ($) {

    $.fn.TableResumeSupport =  {
		
        Resize: function (elem, ancho_resumen) {
            gap = 10;

			tableContenedor = $(elem.closest(".table-container")[0]) ;
			
			pcontenedor = $( $(tableContenedor).parent()) ;
			ancho_total = pcontenedor.width() ;			
			
			tabla = $(".table_with-resumen", pcontenedor) ;
			resumen = $(".table-resumen", pcontenedor) ;
			
			
			alto = tabla.height();
			
			resumen.height(alto);
            resumen.width(ancho_resumen + 'px');

            tabla.attr('style', 'max-width:' + (ancho_total - ancho_resumen - gap) + 'px;');
            // tabla.width((ancho_total - ancho_resumen-gap) + 'px');
			
		}, 

        AnimatePage: function ( render, elem ) {
            $("td,th", elem).hover(
                function () {

                    eval(render + '($(this))'); 

                    $(this).closest("tr").addClass("hover");
                    try {

                        var t = $(this).prop('class').match(/col_([0-9]+)/);

                        if (t &&  t.length >=1) {
                            var id = t[1];
                            $(".col_" + id).addClass("hover");
                        }
                    }
                    catch (err) {
                        console.log(err);
                    }
                },
                function () {

                    fnRender_clear( $(this)); 


                    $(this).closest("tr").removeClass("hover");


                    try {
                        var t = $(this).prop('class').match(/col_([0-9]+)/);

                        if (t && t.length >= 1) {
                            var id = t[1];
                            $(".col_" + id).removeClass("hover");
                        }
                    }
                    catch (err) {
                        console.log(err);
                    }
                }
            )
        }
        ,

        ApplyFormat : function (elem) {
            $(".Number", elem).each(function () {
                $(this).html(fmt_number($(this).html()));
            });
            $(".Currency", elem).each(function () {
                $(this).html(fmt_currency($(this).html()));
            });
            $(".Percent", elem).each(function () {
                $(this).html(fmt_percent($(this).html()));
            });
        }
        ,

		Do : function( action ,args ) {
			
        }


		
    };


    $.fn.TableResume = function (options) {

        // This is the easiest way to have default options.
        var settings = $.extend({
            ancho: 200
        }, options);

    
        return this.each(function () {

            console.log( settings );

            var elem = $(this);

            elem.wrap("<div class='table-container'></div>")
                .after("<div class='table-resumen'><div class='table-resumen-toolbar'></div><div class='table-resumen-content'></div></div>")
                .wrap("<div class='table_with-resumen'></div>");

            $.fn.TableResumeSupport.Resize(elem, settings.ancho);
            $.fn.TableResumeSupport.AnimatePage(settings.render, elem);
            $.fn.TableResumeSupport.ApplyFormat(elem);
            
            	
        });

    };



}(jQuery));