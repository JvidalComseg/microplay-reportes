﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace WebReportes.Utils
{
    public static class SessionEx
    {
        public static string eCommerce = ClaimTypes.StateOrProvince;
        public static string eCommerceName = ClaimTypes.Gender;

        public static string UserName = ClaimTypes.Name ;
        public static string GivenName = ClaimTypes.GivenName;
        public static string SurName = ClaimTypes.Surname;
        public static string FullName = ClaimTypes.Country;

        public static string FlexUserName = ClaimTypes.Actor;
        public static string Roles = ClaimTypes.Role;




        public static bool Register( string _user_name, string _given_name, string _sur_name, string _Flex_user_name, string _roles )
        {
            var ctx = HttpContext.Current.GetOwinContext();

            // Initialization.    
            var claims = new List<Claim>();
            try
            {
                string _full_name = string.Format("{0} {1}", _given_name, _sur_name);

                // Setting    
                claims.Add(new Claim(UserName, _user_name));
                claims.Add(new Claim(GivenName, _given_name));
                claims.Add(new Claim(SurName, _sur_name));
                claims.Add(new Claim(FullName, _full_name));

                claims.Add(new Claim(FlexUserName, _Flex_user_name));

                claims.Add(new Claim(Roles, _roles));
                claims.Add(new Claim(eCommerce, string.Empty ));


                var claimIdenties = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                var authenticationManager = ctx.Authentication;

                // Sign In.    
                authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, claimIdenties);

                return true;
            }
            catch (Exception ex) { }

            return false;
        }

 
        public static void Set( string key ,string value)
        {
            try
            {
                var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                var identity = (ClaimsIdentity)(authenticationManager.User.Identity);

                var claim = identity.Claims.FirstOrDefault(p => p.Type == key);

                if( claim != null )
                    identity.RemoveClaim(claim);

                identity.AddClaim(new Claim(key, value));


                authenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties() { IsPersistent = false });


            }
            catch (Exception ex) { }

        }

        public static string Get(string key)
        {
            try
            {
                var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                var identity = (ClaimsIdentity)(authenticationManager.User.Identity);

                var claim = identity.Claims.FirstOrDefault(p => p.Type == key);

                if (claim != null)
                {
                    return claim.Value;
                }

            }
            catch (Exception ex) { }

            return string.Empty;
        }


        public static Dictionary<string, string> Info()
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            ret["eCommerce"] =  Get(eCommerce);
            ret["UserName"] = Get(UserName);
            ret["GivenName"] = Get(GivenName);
            ret["SurName"] = Get(SurName);
            ret["FullName"] = Get(FullName);
            ret["AltUserName"] = Get(FlexUserName);
            ret["Roles"] = Get(Roles);

            return ret;
        }


    }
}