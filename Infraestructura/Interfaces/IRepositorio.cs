﻿using System.Collections.Generic;

namespace Infraestructura.Interfaces
{
    public interface IRepositorio<T> where T : class
    {
        ICollection<T> Obtener();
        T Obtener(int id);
        T Registrar(T entidad);
        void Eliminar(T entidad);
        void Actualizar(T entidad);
    }
}
