﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;

namespace Infraestructura.Procedimientos
{
    public class RSPSample : RepositorioBase
    {
        public RSPSample() : base() { }


        public IEnumerable<Dictionary<string, string>> Obtener(string sp_name, Dictionary<string, object> Parameters=null)
        {
            try {

                IEnumerable<Dictionary<string, string>> __data = Call_SP(sp_name, Parameters);
                
                return __data;
            }
            catch ( Exception ex )
            {
                string error  =  ex.Message ;
            }
            return null;
        }


    }
}
