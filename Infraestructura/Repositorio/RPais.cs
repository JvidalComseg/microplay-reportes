﻿using Dominio.Entidades;
using Infraestructura.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Infraestructura.Repositorio
{
    public class RPais : RepositorioBase , IRepositorio<Pais>
    {
        public RPais() : base() { }

        public void Actualizar(Pais entidad)
        {
            db.Entry(entidad).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Eliminar(Pais entidad)
        {
            db.Paises.Remove(entidad);
            db.SaveChanges();
        }

        public ICollection<Pais> Obtener()
        {
            return db.Paises.OrderBy(q => q.Nombre).ToList();
        }

        public Pais Obtener(int id)
        {
            return db.Paises.Find(id);
        }

        public Pais Registrar(Pais entidad)
        {
            db.Paises.Add(entidad);
            db.SaveChanges();
            return entidad;
        }
    }
}
