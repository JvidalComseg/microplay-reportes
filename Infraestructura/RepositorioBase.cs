﻿using Dominio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;


namespace Infraestructura
{
    public class RepositorioBase
    {

        public RepositorioBase()
        {
        }

        public  IEnumerable<Dictionary<string, string>> Call_SP(string Sql, Dictionary<string, object> Parameters = null)
        {
                using (var db = new BDDContext())
                {

                    using (var cmd = db.Database.Connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = Sql;
                        cmd.CommandTimeout = 6000;   //  Maximo 10 minutos 
                        if (cmd.Connection.State != ConnectionState.Open)
                            cmd.Connection.Open();

                        if (Parameters != null)
                        {
                            foreach (KeyValuePair<string, object> param in Parameters)
                            {
                                DbParameter dbParameter = cmd.CreateParameter();
                                dbParameter.ParameterName = param.Key;
                                dbParameter.Value = param.Value;
                                cmd.Parameters.Add(dbParameter);

                            }
                        }

                        using (var dataReader = cmd.ExecuteReader())
                        {
                            do
                            {
                                while (dataReader.Read())
                                {
                                    var dataRow = GetDataRow(dataReader);
                                    yield return dataRow;
                                }

                            } while (dataReader.NextResult());
                        }
                    }

                }


        }

    

        protected static  Dictionary<string, string> GetDataRow(DbDataReader dataReader)
        {
            var dataRow = new Dictionary<string, string>();

            for (var fieldCount = 0; fieldCount < dataReader.FieldCount; fieldCount++)
                dataRow.Add(dataReader.GetName(fieldCount), dataReader[fieldCount].ToString());
            return dataRow;
        }

    }
}
