﻿//  using Dominio.Entidades;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

// using Dominio.Entidades;

namespace Dominio
{
    /// <summary>
    /// SigEntidades corresponde la nombre de la conexión del web.config
    /// </summary>
    public class BDDContext : DbContext
    {
        //Configuramos que LazyLoading este deshabilitado por defecto
        public BDDContext(bool lazyLoadingEnabled = false)
        {
            this.Configuration.LazyLoadingEnabled = lazyLoadingEnabled;
        }

        /// <summary>
        /// Propiedad que permitirá acceder a la información de la entidada Pais
        /// </summary>
        // public DbSet<Pais> Paises { get; set; }

        public virtual void Commit() { base.SaveChanges(); }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;

            //Crea la base de datos cada vez que se ejecuta la aplicación
            //Database.SetInitializer(new DropCreateDatabaseAlways<IayEntidades>());

            //Crea la base de datos cada vez que se ejecuta la aplicación si existen cambios
            // Database.SetInitializer(new DropCreateDatabaseIfModelChanges<IayEntidades>());

            //Remueve el plurar de las tablas generado automaticamente.
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
