﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    public class Pais
    {
        public Pais() { }

        [ScaffoldColumn(false)]
        [Display(Name = "IdPais")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PaisId { get; set; }

        [Display(Name = "Nombre Pais")]
        [Required(ErrorMessage = "El nombre del pais es requerido.")]
        [StringLength(150, ErrorMessage = "Maximo permitido 150 caracteres.")]
        [MinLength(3, ErrorMessage = "Minimo...")]
        [Column(TypeName = "varchar")]
        public string Nombre { get; set; }
    }
}
